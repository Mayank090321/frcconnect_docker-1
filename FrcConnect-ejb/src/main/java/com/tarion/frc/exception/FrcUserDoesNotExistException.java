/* 
 * 
 * FrcDatabaseException.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.exception;

import org.apache.commons.lang3.StringUtils;

/**
 * Exception that occurs during FRC database interaction
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
public class FrcUserDoesNotExistException extends RuntimeException {
    
       /** Serial version ID */
       private static final long serialVersionUID = 1L;

       /** Error code */
       protected String errorCode = null;

       /**
        * Instantiates a new exception.
        */
       public FrcUserDoesNotExistException() {
       }

       /**
        * Instantiates a new exception.
        *
        * @param msg the msg
        */
       public FrcUserDoesNotExistException(String msg) {
           super(msg);
       }

       /**
        * Instantiates a new exception.
        *
        * @param msg the msg
        * @param errorCode the error code
        */
       public FrcUserDoesNotExistException(String msg, String errorCode) {
           super(msg);
           this.errorCode = errorCode;
       }

       /**
        * Instantiates a new exception.
        *
        * @param msg the msg
        * @param errorCode the error code
        * @param cause the cause
        */
       public FrcUserDoesNotExistException(String msg, String errorCode, Throwable cause) {
           super(msg, cause);
           this.setStackTrace(cause.getStackTrace());
           this.errorCode = errorCode;
       }

       /**
        * Instantiates a new exception.
        *
        * @param errorCode the error code
        * @param cause the cause
        */
       public FrcUserDoesNotExistException(String errorCode, Throwable cause) {
           super(cause);
           this.setStackTrace(cause.getStackTrace());
           this.errorCode = errorCode;
       }

       /**
        * Gets the error code.
        *
        * @return the error code
        */
       public String getErrorCode() {
           return errorCode;
       }

       /**
        * Return the error code number, by first stripping off the beginning value
        * <code>FRC-</code>.
        *
        * @return Error code number (eg. BSA-99999, will return 99999)
        */
       public int getErrorCodeNumber() {
           int errorCodeNumber = 0;
           if (StringUtils.startsWith(errorCode, "FRC-")) {
        	   final String code = StringUtils.substringAfter(errorCode, "FRC-");
               try {
                   errorCodeNumber = Integer.parseInt(code);
               } catch (NumberFormatException nfe) {
                   // Default to negative one if bad error code number
                   errorCodeNumber = -1;
               }
           }
           return errorCodeNumber;
       }

       /* (non-Javadoc)
        * @see java.lang.Throwable#toString()
        */
       @Override
       public String toString() {
           StringBuilder buf = new StringBuilder();
           if (errorCode != null) {
               buf.append(errorCode);
               buf.append(": ");
           }
           buf.append(super.toString());
           return buf.toString();
       }
}