/* 
 * 
 * FrcProperties.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.service;

import javax.ejb.Remote;

/**
 * This class is a singleton used for Frc Properties
 * Properties can be stored in the file defined by
 * FrcConstants.FRC_PROPERTIES_FILE or in the FRC_CONFIG table in FRC database
 * 
 * @author <a href="bojan.volcansek@tarion.com">Bojan Volcansek</a>
 * @date 2016-02-09
 * @version 1.0
 */
@Remote
public interface FrcProperties {
	String getValue(String name);
}
