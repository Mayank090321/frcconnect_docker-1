/* 
 * 
 * WebServiceInvocationBean.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.service;

import java.net.MalformedURLException;
import java.net.URL;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.namespace.QName;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.exception.FrcWebServiceException;
import com.tarion.frc.util.LoggerUtil;
import com.tarion.tbi.cm.attachments.Attachments;
import com.tarion.tbi.cm.attachments.AttachmentsService;
import com.tarion.tbi.cm.attachments.AttachmentsWSBeanService;
import com.tarion.tbi.cm.attachments.SubmissionResponse;
import com.tarion.tbi.esp.service.EmailSender;
import com.tarion.tbi.esp.service.EmailSenderWSBean;
import com.tarion.tbi.services.FRCConnectService;
import com.tarion.tbi.services.FRCConnectServiceImplService;
import com.tarion.tbi.services.FrcEnrolmentInfoResponse;


/**
 * WebService Invocation Bean - invokes different Web Services
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-03-03
 * @version 1.0
 */
@Stateless(mappedName = "ejb/WebServiceInvocationBeanRemote")
@LocalBean
public class WebServiceInvocationBean implements WebServiceInvocationBeanRemote {

	@EJB
	private FrcProperties propertyService;

	//	@EJB no EJB annotation because this is only interface without implementation
	private EmailSenderWSBean emailSenderWebService;
	private AttachmentsService uploadAttachmentsWebService;
	private FRCConnectService frcConnectWebService;
	
	public void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws FrcWebServiceException {
		try {
			getEmailSenderWebService().sendEmailWithText(subject, body, fromAddress, toAddresses, null, null, false, null, null, null);
		} catch (Exception e) {
			String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailWithText";
			LoggerUtil.logError(WebServiceInvocationBean.class, exceptionMessage, e);
			throw new FrcWebServiceException(e.getMessage(), e);
		}
	}
	
	public SubmissionResponse uploadFiles(Attachments attachments, String userName) throws FrcWebServiceException {
		long startTime = LoggerUtil.logEnter(WebServiceInvocationBean.class, "uploadFiles", attachments, userName);
		String clientTrackingNumber = String.valueOf(System.currentTimeMillis());
		
		SubmissionResponse ret = null;
		try {
			AttachmentsService service = getAttachmentsService();
			ret = service.uploadWithUsername(FrcConstants.FRC_CONNECT_CM_UPLOAD_SOURCE_SYSTEM, clientTrackingNumber, attachments, userName);
		} catch (Throwable th) {
			LoggerUtil.logServiceError(WebServiceInvocationBean.class, "uploadFiles", th, attachments, userName);
			throw new FrcWebServiceException("Upload to CM failed for tracking num  " + clientTrackingNumber + ". "
					+ th.getMessage(), th);
		}
		LoggerUtil.logExit(WebServiceInvocationBean.class, "uploadFiles", ret, startTime);
		return ret;
	}
	
	public FrcEnrolmentInfoResponse getEnrolmentInfo(String enrolmentId) throws FrcWebServiceException {
		long startTime = LoggerUtil.logEnter(WebServiceInvocationBean.class, "getEnrolmentInfo", enrolmentId);
		String clientTrackingNumber = String.valueOf(System.currentTimeMillis());
		
		FrcEnrolmentInfoResponse ret = null;
		try {
			FRCConnectService service = getTIPFRCService();
			ret = service.getFRCEnrolmentInfo(enrolmentId);
		} catch (Throwable th) {
			LoggerUtil.logServiceError(WebServiceInvocationBean.class, "getEnrolmentInfo", th, enrolmentId);
			throw new FrcWebServiceException("Upload to CM failed for tracking num  " + clientTrackingNumber + ". "
					+ th.getMessage(), th);
		}
		LoggerUtil.logExit(WebServiceInvocationBean.class, "getEnrolmentInfo", ret, startTime);
		return ret;
	}
	
//	public void sendEmailUsingEspWithFile(String subject, String body, String fromAddress, String toAddresses, byte[] emailAttachment, String fileName, String mimeType) throws BsaException {
//		try {
//			getEmailSenderWebService().sendEmailWithFile(subject, body, fromAddress, toAddresses, null, null, emailAttachment, fileName, mimeType, false, null, null, null);
//		} catch (Exception e) {
//			String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailWithFile";
//			logger.error(exceptionMessage, e);
//			throw new BsaException(e.getMessage(), e);
//		}
//	}
//	
//	public void sendEmailUsingEspWithListOfFiles(String subject, String body, String fromAddress, String toAddresses, List<EmailAttachmentDto> emailAttachments) throws BsaException { 
//		try {
//			List<com.tarion.tbi.esp.emailattachment.EmailAttachmentDto> webServiceList = new ArrayList<com.tarion.tbi.esp.emailattachment.EmailAttachmentDto>();
//			for (EmailAttachmentDto dto : emailAttachments) {
//				com.tarion.tbi.esp.emailattachment.EmailAttachmentDto webServiceDto = new com.tarion.tbi.esp.emailattachment.EmailAttachmentDto();
//				webServiceDto.setDocData(dto.getDocData());
//				webServiceDto.setFilename(dto.getFilename());
//				webServiceDto.setMimeFileType(dto.getMimeFileType());
//				webServiceList.add(webServiceDto);
//			}
//			getEmailSenderWebService().sendEmailWithListOfFiles(subject, body, fromAddress, toAddresses, null, null, webServiceList, false, null, null, null);
//		} catch (Exception e) {
//			String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailUsingEspWithListOfFiles";
//			logger.error(exceptionMessage, e);
//			throw new BsaException(e.getMessage(), e);
//		}
//	}
//
//	public SubmissionResponse sendEmailUsingEspUploadToCm(String subject, String body, String fromAddress, String toAddresses, List<EmailCmAttribute> emailCmAttributeList, String itemType, String pdfConversionTemplateId) {
//		SubmissionResponse response = null;
//		try {
//			response = getEmailSenderWebService().sendEmailWithText(subject, body, fromAddress, toAddresses, null, null, true, emailCmAttributeList, itemType, pdfConversionTemplateId);
//		} catch (Exception e) {
//			String exceptionMessage = "Exception occured while invoking TBI Web Service EmailSender.sendEmailUsingEspUploadToCm";
//			logger.error(exceptionMessage, e);
//			throw new BsaException(e.getMessage(), e);
//		}
//		
//		return response;
//	}
//

	private EmailSenderWSBean getEmailSenderWebService() throws MalformedURLException {
		if (emailSenderWebService == null) {
			String wsdlUrlString = propertyService.getValue(FrcConstants.FRC_WS_CLIENT_ESP_WEB_SERVICE_WSDL_URL);
			URL webServiceUrl = new URL(wsdlUrlString);
			QName serviceName = new QName("http://service.esp.tbi.tarion.com/", "EmailSender");
	
			EmailSender service = new EmailSender(webServiceUrl, serviceName);
			emailSenderWebService = service.getEmailSenderWSBeanPort();
		}
		return emailSenderWebService;
	}
	
	private AttachmentsService getAttachmentsService() throws MalformedURLException {
		if (uploadAttachmentsWebService == null) {
			String wsdlUrlString = propertyService.getValue(FrcConstants.FRC_WS_CLIENT_TBI_UPLOAD_ATTACHMENTS_WSDL_URL);
			QName qName = new QName("http://attachments.cm.tbi.tarion.com/", "AttachmentsWSBeanService");
			URL url = new URL(wsdlUrlString);

			AttachmentsWSBeanService service = new AttachmentsWSBeanService(url, qName);
			uploadAttachmentsWebService = service.getAttachmentsServicePort();
		}
		return uploadAttachmentsWebService;
	}
	
	public FRCConnectService getTIPFRCService() throws MalformedURLException {
		if (frcConnectWebService == null) {
			String wsdlUrlString = propertyService.getValue(FrcConstants.FRC_WS_CLIENT_TBI_FRC_WSDL_URL);
			QName qName = new QName("http://services.tbi.tarion.com/", "FRCConnectServiceImplService");
			URL url = new URL(wsdlUrlString);

			FRCConnectServiceImplService service = new FRCConnectServiceImplService(url, qName);
			frcConnectWebService = service.getFRCConnectServicePort();
		}
		return frcConnectWebService;
	}
}
