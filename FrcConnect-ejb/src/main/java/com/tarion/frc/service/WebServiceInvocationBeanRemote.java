/* 
 * 
 * WebServiceInvocationBeanRemote.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.service;

import java.net.MalformedURLException;

import javax.ejb.Remote;

import com.tarion.frc.exception.FrcWebServiceException;
import com.tarion.tbi.cm.attachments.Attachments;
import com.tarion.tbi.cm.attachments.SubmissionResponse;
import com.tarion.tbi.services.FRCConnectService;
import com.tarion.tbi.services.FrcEnrolmentInfoResponse;

/**
 * WebService Invocation Bean - invokes different Web Services
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-03-03
 * @version 1.0
 */
@Remote
public interface WebServiceInvocationBeanRemote {
	public SubmissionResponse uploadFiles(Attachments attachments, String userName) throws FrcWebServiceException;
	public void sendEmailUsingEsp(String subject, String body, String fromAddress, String toAddresses) throws FrcWebServiceException;
	public FrcEnrolmentInfoResponse getEnrolmentInfo(String enrolmentId) throws FrcWebServiceException;
//	public void sendEmailUsingEspWithFile(String subject, String body, String fromAddress, String toAddresses, byte[] emailAttachment, String fileName, String mimeType) throws FrcWebServiceException;
//	public void sendEmailUsingEspWithListOfFiles(String subject, String body, String fromAddress, String toAddresses, List<EmailAttachmentDto> emailAttachments) throws FrcWebServiceException; 
//
//	public SubmissionResponse sendEmailUsingEspUploadToCm(String subject, String body, String fromAddress, String toAddresses, List<EmailCmAttribute> emailCmAttributeList, String itemType, String pdfConversionTemplateId);
//
//	public byte[] convertXhtmlToPdf(String xhtml);
//	public byte[] convertTemplateToPdf(List<Object> listOfTemplateArguments, String templateId);
}
