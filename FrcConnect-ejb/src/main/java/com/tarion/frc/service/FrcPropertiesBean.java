/* 
 * FrcPropertiesBean
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 */
package com.tarion.frc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tarion.common.TarionEnvConfigRemote;
import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.entity.FrcConfigEntity;
import com.tarion.frc.exception.PropertiesLoadingException;
import com.tarion.frc.util.LoggerUtil;

/**
 * This class is a singleton used for Frc Properties
 * Properties can be stored in the file defined by
 * FrcConstants.FRC_PROPERTIES_FILE or in the FRC_CONFIG table in FRC database
 * 
 * @author <a href="bojan.volcansek@tarion.com">Bojan Volcansek</a>
 * @date 2016-02-09
 * @version 1.0
 */
@Startup
@Singleton(mappedName = "ejb/FrcProperties")
public class FrcPropertiesBean implements FrcProperties {
	/** ESP Properties */
	private static Properties prop = null;
	
	@EJB(name = "TarionEnvConfigRemote")
	TarionEnvConfigRemote tarionEnvConfig;

	private String environmentDomain = null;

	private static final String ENV_DOMAIN = "env.domain";

	/** Entity Manger injection */
	@PersistenceContext(unitName = "frc")
	private EntityManager frcEm;

	private static String JPQL_GET_ALL_FRC_DB_PROPERTIES = " select p from FrcConfigEntity p order by p.name";

	@PostConstruct
	void startupEjb() throws PropertiesLoadingException {
		initTarionEnvironmentProperties();
		initDbProperties();
	}
	
	private void initTarionEnvironmentProperties() {
		prop = tarionEnvConfig.getEnvironmentPropertiesForApplication("frc");

		environmentDomain = prop.getProperty(ENV_DOMAIN);

	}

	private void initDbProperties() throws PropertiesLoadingException {
		if (prop == null) {
			initTarionEnvironmentProperties();
		}
		
		List<FrcConfigEntity> dbPropertyList = new ArrayList<FrcConfigEntity>();
		try {
			Query query = frcEm.createQuery(JPQL_GET_ALL_FRC_DB_PROPERTIES);
			dbPropertyList = query.getResultList();

			if (dbPropertyList == null || dbPropertyList.isEmpty()) {
				LoggerUtil.logWarn(FrcPropertiesBean.class, " FRC_CONFIG table is empty");
			} else {
				for (FrcConfigEntity dbProperty : dbPropertyList) {
					prop.setProperty(dbProperty.getName(), dbProperty.getValue());
				}
			}

		} catch (Exception ex) {
			String errorMessage = "Error reading property values from FRC_CONFIG table";
			LoggerUtil.logError(FrcPropertiesBean.class, errorMessage, ex);
			throw new PropertiesLoadingException(errorMessage, ex);
		}
	}
	
	public String getValue(String name) {
		if (prop == null) {
			startupEjb();
		}
		if (FrcConstants.FRC_EMAIL_FRC_CONNECT_APP_URL.equalsIgnoreCase(name) ||
				FrcConstants.FRC_WS_CLIENT_ESP_WEB_SERVICE_WSDL_URL.equalsIgnoreCase(name) ||
				FrcConstants.FRC_WS_CLIENT_TBI_FRC_WSDL_URL.equalsIgnoreCase(name) ||
				FrcConstants.FRC_WS_CLIENT_TBI_UPLOAD_ATTACHMENTS_WSDL_URL.equalsIgnoreCase(name)) {
			return getValue(name, environmentDomain);
		} 
		return prop.getProperty(name);
	}
	
	/**
	 * Method to get value of property key with list of values to format the string.
	 *
	 * @param key    the key
	 * @param values The values to substitute into the string
	 * @return The property, null if not found
	 */
	private String getValue(String key, String... values) {

		String tempString = prop.getProperty(key);

		if (tempString != null && values != null) {
			for (int i = 0; i < values.length; i++) {
				tempString = tempString.replaceAll("\\{" + i + "\\}", values[i]);
			}
		}
		return tempString;
	}


}
