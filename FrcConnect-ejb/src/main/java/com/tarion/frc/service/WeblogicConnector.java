package com.tarion.frc.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

import org.apache.commons.lang3.StringUtils;

import com.tarion.frc.exception.FrcLdapException;
import com.tarion.frc.util.LoggerUtil;

/**
 * This is the JMS connector class used to get stats from Weblogic Admin Server.
 * 
 * Copied from OpenMQConnector created by Paul.Richardson.
 * Updated to 
 * 
 * @author abasaric
 * 
 */
public class WeblogicConnector {

	private static Map<String, WeblogicConnector> serverList = new HashMap<>();

	private static final String T3_PROTOCOL = "t3";
	
	private MBeanServerConnection mbsc;
	
	private boolean gotClusterInfo;

	// as opposed to isProductionServer
	// defaults to false (i.e. assume production) until we prove otherwise 
	private boolean isNonProductionServer;

	private static final ObjectName service;

	static {
		try {
			service = new ObjectName(
					"com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean");
		} catch (MalformedObjectNameException e) {
			throw new AssertionError(e.getMessage());
		}
	}

	/**
	 * Create a connection to the specified server
	 * 
	 * @param connectionString
	 *            The connection string to use to connect to the MQ Broker
	 * @throws PIAdminException
	 */
	public WeblogicConnector(String connectionString, String username, String password){// throws Exception {
		long startTime = LoggerUtil.logEnter(WeblogicConnector.class, "WeblogicConnector", connectionString);

		if (!StringUtils.contains(connectionString, ':')) {
			LoggerUtil.logError(WeblogicConnector.class, "Invalid connection string. Must contain server name and port separated by a colon: " + connectionString);
		}
		
		String[] temp = StringUtils.split(connectionString, ":");
		String protocol = temp[0];
		String hostname = StringUtils.remove(temp[1], "/");
		String portString = temp[2];
		
		try {
			initConnection(protocol, hostname, portString, username, password);
		} catch (Exception e) {
			throw new FrcLdapException("Problem connecting to WLS AdminServer", e);
		}

		LoggerUtil.logExit(WeblogicConnector.class, "WeblogicConnector", startTime);
	}

	/**
	 * Get instance method allows you to retrieve a copy of the OpenMQ JMX
	 * object to either PIT or HOP
	 * 
	 * @param connectionString
	 *            The connection string, either to PIT or to HOP
	 * @return An object pointing to either PIT or HOP
	 * @throws PIAdminException
	 */
	public static synchronized WeblogicConnector getInstance(String connectionString, String username, String password){// throws Exception {
		if (serverList.get(connectionString) == null) {
			serverList.put(connectionString, new WeblogicConnector(connectionString, username, password));
		}
		return serverList.get(connectionString);
	}

	private void initConnection(String protocol, String hostname, String portString, String username, String password)
			throws IOException, MalformedURLException {

		long startTime = LoggerUtil.logEnter(WeblogicConnector.class, "initConnection", protocol, hostname, portString, username, password);
		
		if (!T3_PROTOCOL.equalsIgnoreCase(protocol)){
			throw new IllegalArgumentException("initConnection - protocol not supported: " + protocol);
		}
		
		Integer portInteger = Integer.valueOf(portString);
		int port = portInteger.intValue();
		String jndiroot = "/jndi/";

		String mserver = "weblogic.management.mbeanservers.runtime";

		JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + mserver);

		Hashtable<String, Object> h = new Hashtable<>();
		h.put(Context.SECURITY_PRINCIPAL, username);
		h.put(Context.SECURITY_CREDENTIALS, password);
		h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
		h.put("jmx.remote.x.request.waiting.timeout", new Long(10000));
		JMXConnector connector = JMXConnectorFactory.connect(serviceURL, h);
		mbsc = connector.getMBeanServerConnection();

		LoggerUtil.logInfo(WeblogicConnector.class, "default Domain: " + mbsc.getDefaultDomain());
		LoggerUtil.logInfo(WeblogicConnector.class, "mbean count: " + mbsc.getMBeanCount());
		LoggerUtil.logInfo(WeblogicConnector.class, "Domains: " + mbsc.getDomains());
		for (String domain : mbsc.getDomains()) {
			LoggerUtil.logInfo(WeblogicConnector.class, "Domains: " + domain);
		}
		
		LoggerUtil.logExit(WeblogicConnector.class, "initConnection", startTime);
	}

	public void closeConnector() {
		// connector.close();
	}

	// Example from stackoverflow
	// http://stackoverflow.com/questions/22173512/accessing-weblogic-jms-using-java-and-jmx-mbeans
	public void countMessages() throws Exception {
		ObjectName serverRuntime = (ObjectName) mbsc.getAttribute(service, "ServerRuntime");
		ObjectName jmsRuntime = (ObjectName) mbsc.getAttribute(serverRuntime, "JMSRuntime");
		ObjectName[] jmsServers = (ObjectName[]) mbsc.getAttribute(jmsRuntime, "JMSServers");
		for (ObjectName jmsServer : jmsServers) {
			//String srvName = jmsServer.getKeyProperty("Name");
			ObjectName[] destinations = (ObjectName[]) mbsc.getAttribute(jmsServer, "Destinations");
			for (ObjectName destination : destinations) {
				String queueName = destination.getKeyProperty("Name");
				Long queueDepth = (Long) mbsc.getAttribute(destination, "MessagesCurrentCount");
				LoggerUtil.logDebug(WeblogicConnector.class, "Queue: " + queueName + " Depth: " + queueDepth);
			}
			break;
		}
	}

	private void getClusterInfo() throws Exception {
		ObjectName domain1 = (ObjectName) mbsc.getAttribute(service, "DomainConfiguration");
		ObjectName[] cluster_list = (ObjectName[]) mbsc.getAttribute(domain1, "Clusters");
		for (ObjectName cl : cluster_list) {
			LoggerUtil.logInfo(WeblogicConnector.class, 
					"######################## \n Cluster Name: " + (String) mbsc.getAttribute(cl, "Name"));
			ObjectName[] servers = (ObjectName[]) mbsc.getAttribute(cl, "Servers");
			for (ObjectName ser : servers) {
				String listenAddress = (String) mbsc.getAttribute(ser, "ListenAddress");
				Integer listenPort = (Integer) mbsc.getAttribute(ser, "ListenPort");
				LoggerUtil.logInfo(WeblogicConnector.class, "Address: " + listenAddress + "\t Port: " + listenPort);
				if (!StringUtils.startsWith(StringUtils.lowerCase(listenAddress), "prd")) {
					isNonProductionServer = true;					
				}
			}
			LoggerUtil.logInfo(WeblogicConnector.class, "######################## \n");
		}
		LoggerUtil.logInfo(WeblogicConnector.class, "");
	}

	public boolean isNonProductionServer() throws Exception {
		if (!gotClusterInfo) {
			getClusterInfo();
			gotClusterInfo = true;
		}
		return isNonProductionServer;
	}

	public boolean isProductionServer() throws Exception {
		return !isNonProductionServer();
	}
}
