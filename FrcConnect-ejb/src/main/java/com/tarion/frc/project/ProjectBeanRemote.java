/* 
 * 
 * ProjectBeanRemote.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.project;

import java.util.List;

import javax.ejb.Remote;

import com.tarion.frc.common.dto.DocumentUploadDto;
import com.tarion.frc.common.dto.NameValueDto;
import com.tarion.frc.project.dto.PreviousDocumentsDto;
import com.tarion.frc.project.dto.ProjectDto;
import com.tarion.tbi.cm.attachments.SubmissionResponse;
import com.tarion.tbi.services.FrcEnrolmentInfoResponse;

/**
 * FRC Project Bean.
 * Operations for search CRM for projects associated with FRC USer / Company
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-19
 * @version 1.0
 */
@Remote
public interface ProjectBeanRemote {
	public List<ProjectDto> searchForProjectsLikeAddress(String companyId, String addressStreetNumber, String addressStreetName);
	public List<ProjectDto> searchForProjectsLikeProjectName(String companyId, String projectName);
    public SubmissionResponse uploadFilesToCmAndSendCrmWorklist(String userName, String vbNumber, String enrolmentNumber, List<DocumentUploadDto> fileList);
	public List<NameValueDto> getAllB19TemplateTypes();
	public int getCurrentSixtyDayReportNumberForEnrolment(String enrolmentId);
	List<ProjectDto> searchForProjectsLikeEnrolmentId(String enrolmentId);
	FrcEnrolmentInfoResponse getEnrolmentInfo(String enrolmentid);
	public List<PreviousDocumentsDto> getPreviousDocuments(String enrolmentNumber);
}
