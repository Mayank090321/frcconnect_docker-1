/* 
 * 
 * ProjectBean.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.common.dto.DocumentUploadDto;
import com.tarion.frc.common.dto.NameValueDto;
import com.tarion.frc.entity.FrcCrmB19TemplateTypeEntity;
import com.tarion.frc.entity.FrcCrmSiteEntity;
import com.tarion.frc.entity.FrcCrmSiteSixtyDayReportEntity;
import com.tarion.frc.entity.FrcPreviousDocumentEntity;
import com.tarion.frc.exception.FrcDatabaseException;
import com.tarion.frc.project.dto.PreviousDocumentsDto;
import com.tarion.frc.project.dto.ProjectDto;
import com.tarion.frc.service.FrcProperties;
import com.tarion.frc.service.WebServiceInvocationBeanRemote;
import com.tarion.frc.util.FrcUtil;
import com.tarion.frc.util.LoggerUtil;
import com.tarion.tbi.cm.attachments.Attachment;
import com.tarion.tbi.cm.attachments.Attachments;
import com.tarion.tbi.cm.attachments.CmAttribute;
import com.tarion.tbi.cm.attachments.CmAttributes;
import com.tarion.tbi.cm.attachments.SubmissionResponse;
import com.tarion.tbi.services.FrcEnrolmentInfoResponse;

/**
 * Project Bean.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/ProjectBeanRemote")
@LocalBean
public class ProjectBean implements ProjectBeanRemote {
	private static final String JPQL_CRM_GET_PROJECTS_LIKE_PROJECT_NAME =
			"select u from FrcCrmSiteEntity u " +
			"where UPPER(u.projectName) like UPPER(:projectName) "
			+ " and u.companyId = :companyId order by u.siteId ";
	
	private static final String JPQL_CRM_GET_PROJECTS_LIKE_ADDRESS =
			"select u from FrcCrmSiteEntity u " +
			"where UPPER(u.siteAddress1) like UPPER(:siteAddress1) "
			+ " and u.companyId = :companyId order by u.siteId ";
	
	private static final String JPQL_CRM_GET_PROJECTS_LIKE_ENROLMENT =
			"select u from FrcCrmSiteEntity u " +
			"where u.siteId = :enrolmentId ";
	
	private static final String JPQL_CRM_GET_ALL_B19_TEMPLATE_TYPES = 
			"select u from FrcCrmB19TemplateTypeEntity u order by u.tempateTypeCode ";

	private static final String JPQL_CRM_GET_CURRENT_SIXTY_DAY_REPORT_FOR_ENROLMENT = 
			"select u from FrcCrmSiteSixtyDayReportEntity u "
			+ " where u.siteId = :siteId ";
	
	private static final String JPQL_CRM_GET_PREVIOUS_DOCUMENTS = 
			"select u from FrcPreviousDocumentEntity u "
			+ " where u.siteId = :siteId "
			+ " order by u.siteId, u.seqNum ";

	
	@EJB
	private FrcProperties propSvc;
	@EJB
	private WebServiceInvocationBeanRemote webService;
    
    /** FRC Persistence Context */
    @PersistenceContext(unitName = "frc")
    private EntityManager frcEm;

    /** CRM Persistence Context */
    @PersistenceContext(unitName = "crm")
    private EntityManager crmEm;

    /**
     * Default constructor.
     */
    public ProjectBean() {
    }
    
    public SubmissionResponse uploadFilesToCmAndSendCrmWorklist(String userName, String vbNumber, String enrolmentNumber, List<DocumentUploadDto> fileList) {
    	SubmissionResponse response = null;
    	if (fileList != null && fileList.size() > 0) {
	    	Attachments attachments = new Attachments();
	    	for (DocumentUploadDto file : fileList) {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				String dateFormatted = df.format(new Date());
	
		    	Attachment attachment = new Attachment();
		    	attachment.setBytes(file.getFileData());
		    	attachment.setClientTrackingId(dateFormatted);
		    	attachment.setCmItemType(FrcConstants.FRC_CM_ITEM_TYPE_BB19_DOCUMENT);
		    	attachment.setFileType(file.getMimeFileType());
		
		    	CmAttributes cmAttributes = new CmAttributes();
		    	
		    	CmAttribute cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_VBNUMBER);
		    	cmAttribute.setValue(FrcUtil.removeBFromVB(vbNumber));
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		    	
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_ENROLMENT_NUMBER);
		    	cmAttribute.setValue(FrcUtil.removeHFromEnrolment(enrolmentNumber));
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		    	    	
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_CREATED_DATE);
		    	cmAttribute.setValue(dateFormatted);
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		    	
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_CM_FILE_NAME);
		    	cmAttribute.setValue(file.getFilename());
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		    	
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_TEMPLATE_TYPE);
		    	cmAttribute.setValue(file.getTemplateType().getValue());
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_TEMPLATE_TYPE_CODE);
		    	cmAttribute.setValue(file.getTemplateType().getName());
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		    	
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_CREATED_BY);
		    	cmAttribute.setValue(userName);
		    	cmAttributes.getCmAttribute().add(cmAttribute);
		
		    	cmAttribute = new CmAttribute();
		    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_SCAN_DATE);
		    	cmAttribute.setValue(dateFormatted);
		    	cmAttributes.getCmAttribute().add(cmAttribute);

		    	if (file.getTemplateType().getName().equalsIgnoreCase(FrcConstants.FRC_CM_TEMPLATE_TYPE_CODE_60_DAY_TEMPLATE)) {
			    	cmAttribute = new CmAttribute();
			    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_60_DAY_TEMPLATE_TYPE_START_DATE);
			    	cmAttribute.setValue(df.format(file.getStartDate()));
			    	cmAttributes.getCmAttribute().add(cmAttribute);
			
			    	cmAttribute = new CmAttribute();
			    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_60_DAY_TEMPLATE_TYPE_END_DATE);
			    	cmAttribute.setValue(df.format(file.getEndDate()));
			    	cmAttributes.getCmAttribute().add(cmAttribute);
			
			    	cmAttribute = new CmAttribute();
			    	cmAttribute.setName(FrcConstants.FRC_CM_ATTRIBUTE_NAME_60_DAY_TEMPLATE_TYPE_REPORT_NUMBER);
			    	cmAttribute.setValue(file.getReportNumber());
			    	cmAttributes.getCmAttribute().add(cmAttribute);
		    	}
		    	attachment.setCmAttributes(cmAttributes);
		    	attachments.getAttachment().add(attachment);
	    	}
	    	
	
	    	response = webService.uploadFiles(attachments, userName);
    	}
    	return response;
    }
    
	@Override
	public int getCurrentSixtyDayReportNumberForEnrolment(String enrolmentId) {
		int currentSixtyDayReportNumber = 0;
		Query query = crmEm.createQuery(JPQL_CRM_GET_CURRENT_SIXTY_DAY_REPORT_FOR_ENROLMENT);
		query.setParameter("siteId", enrolmentId);
		
		List<FrcCrmSiteSixtyDayReportEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Error searching CRM 60 Day Reports for enrolment " + enrolmentId + ". Error is " + e.getMessage());
		}
		
		for (FrcCrmSiteSixtyDayReportEntity entity : entityList) {
			if (entity.getReportNumber() != null) {
				try {
					currentSixtyDayReportNumber = Integer.parseInt(entity.getReportNumber());
				} catch (NumberFormatException e) {
					LoggerUtil.logError(ProjectBean.class, "Error parsing Report Number to Integer for enrolment " + enrolmentId + ".", e);
				}
			}
		}

		return currentSixtyDayReportNumber;
	}
	
	@Override
	public List<ProjectDto> searchForProjectsLikeAddress(String companyId, String addressStreetNumber, String addressStreetName) {
		List<ProjectDto> returnList = new ArrayList<ProjectDto>();
		Query query = crmEm.createQuery(JPQL_CRM_GET_PROJECTS_LIKE_ADDRESS);
		query.setParameter("companyId", companyId);
		
		String address = "%";
		if (addressStreetNumber != null) {
			address += addressStreetNumber;
			address += "%";
		}
		if (addressStreetName != null) {
			address += addressStreetName;
			address += "%";
		}
		query.setParameter("siteAddress1", address);
		
		List<FrcCrmSiteEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Error searching CRM Sites for address " + address + ". Error is " + e.getMessage());
		}
		
		for (FrcCrmSiteEntity entity : entityList) {
			ProjectDto dto = convertEntityToDto(entity);
			returnList.add(dto);
		}

		return returnList;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<PreviousDocumentsDto> getPreviousDocuments(String enrolmentNumber) {
		
		Query query = crmEm.createQuery(JPQL_CRM_GET_PREVIOUS_DOCUMENTS);
		query.setParameter("siteId", enrolmentNumber);
		
		List<FrcPreviousDocumentEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Error searching CRM previous documents for site" + enrolmentNumber+ ". Error is " + e.getMessage());
		}
		
		List<PreviousDocumentsDto> returnList = new ArrayList<PreviousDocumentsDto>();
		
		for (FrcPreviousDocumentEntity entity : entityList) {
			PreviousDocumentsDto dto = convertEntityToDto(entity);
			returnList.add(dto);
		}
		
		return returnList;
	}

	private PreviousDocumentsDto convertEntityToDto(FrcPreviousDocumentEntity entity) {
		PreviousDocumentsDto dto = new PreviousDocumentsDto();
		
		dto.setDecision(entity.getDecision());
		
		dto.setDecisionDate(FrcUtil.convertCrmStringDateToDate(entity.getDecisionDate()));
		dto.setDescription(entity.getDescription());
		dto.setOnTime(entity.getOnTime());
		dto.setReceivedDate(FrcUtil.convertCrmStringDateToDate(entity.getReceivedDate()));
		return dto;
	}

	@Override
	public List<ProjectDto> searchForProjectsLikeProjectName(String companyId, String projectName) {
		List<ProjectDto> returnList = new ArrayList<ProjectDto>();
		Query query = crmEm.createQuery(JPQL_CRM_GET_PROJECTS_LIKE_PROJECT_NAME);
		query.setParameter("companyId", companyId);
		query.setParameter("projectName", "%" + projectName + "%");
		
		List<FrcCrmSiteEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Error searching CRM Sites for project name " + projectName + ". Error is " + e.getMessage());
		}
		
		for (FrcCrmSiteEntity entity : entityList) {
			ProjectDto dto = convertEntityToDto(entity);
			returnList.add(dto);
		}

		return returnList;
	}
	
	@Override
	public List<ProjectDto> searchForProjectsLikeEnrolmentId(String enrolmentId) {
		List<ProjectDto> returnList = new ArrayList<ProjectDto>();
		Query query = crmEm.createQuery(JPQL_CRM_GET_PROJECTS_LIKE_ENROLMENT);
		query.setParameter("enrolmentId", enrolmentId);
		
		List<FrcCrmSiteEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Error searching CRM Sites for project enrolment " + enrolmentId + ". Error is " + e.getMessage());
		}
		
		for (FrcCrmSiteEntity entity : entityList) {
			ProjectDto dto = convertEntityToDto(entity);
			returnList.add(dto);
		}

		return returnList;
	}


	private ProjectDto convertEntityToDto(FrcCrmSiteEntity entity) {
		ProjectDto dto = null;
		if (entity != null) {
			dto = new ProjectDto();
			dto.setEnrolmentNumber(entity.getSiteId());
			dto.setProjectName(entity.getProjectName());
			dto.setCivicAddress(entity.getSiteAddress1());
			dto.setVbName(entity.getVbName());
			dto.setVbNumber(entity.getVbNumber());
			dto.setPhaseNumber(entity.getProjectPhase());
		}
		return dto;
	}
	
	
	@Override
	public List<NameValueDto> getAllB19TemplateTypes() {
		List<NameValueDto> returnList = new ArrayList<NameValueDto>();
		Query query = crmEm.createQuery(JPQL_CRM_GET_ALL_B19_TEMPLATE_TYPES);
		
		List<FrcCrmB19TemplateTypeEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Error getting template type from CRM. Error is " + e.getMessage());
		}
		
		for (FrcCrmB19TemplateTypeEntity entity : entityList) {
			NameValueDto dto = new NameValueDto(entity.getTempateTypeCode(), entity.getTemplateType());
			returnList.add(dto);
		}

		return returnList;
	}
	
	@Override
	public FrcEnrolmentInfoResponse getEnrolmentInfo(String enrolmentId){
		return webService.getEnrolmentInfo(enrolmentId);
	}
	
}
