/* 
 * 
 * UserProfileBean.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.userprofile;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.entity.FrcCrmCompanyEntity;
import com.tarion.frc.entity.FrcUserProfileEntity;
import com.tarion.frc.exception.FrcDatabaseException;
import com.tarion.frc.exception.FrcUserAlreadyExistsException;
import com.tarion.frc.exception.FrcUserDoesNotExistException;
import com.tarion.frc.service.FrcProperties;
import com.tarion.frc.service.WebServiceInvocationBeanRemote;
import com.tarion.frc.userprofile.dto.UserProfileDto;
import com.tarion.frc.userprofile.dto.UserProfileDto.RoleType;
import com.tarion.frc.util.FrcUtil;
import com.tarion.frc.util.LoggerUtil;

/**
 * User Profile Bean.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/UserProfileBeanRemote")
@LocalBean
public class UserProfileBean implements UserProfileBeanRemote {
	private static final String JPQL_GET_USER_PROFILE_LIST_FOR_COMPANY_NAME =
			"select u from FrcUserProfileEntity u " +
			"where UPPER(u.companyName) like UPPER(:company)  order by u.companyName, u.userName ";

	private static final String JPQL_CRM_GET_ALL_COMPANIES =
			"select u from FrcCrmCompanyEntity u ";
	@EJB
	private FrcProperties propSvc;
    @EJB
    private LdapSecurityBeanRemote ldapSecurityBean;
    @EJB
    private WebServiceInvocationBeanRemote emailService;
    
    /** FRC Persistence Context */
    @PersistenceContext(unitName = "frc")
    private EntityManager frcEm;

    /** CRM Persistence Context */
    @PersistenceContext(unitName = "crm")
    private EntityManager crmEm;

    /**
     * Default constructor.
     */
    public UserProfileBean() {
    }
    
    public Map<String, String> getAllCrmCompanies() {
    	Map<String, String> map = new HashMap<String, String>();
		Query query = crmEm.createQuery(JPQL_CRM_GET_ALL_COMPANIES);
		
		List<FrcCrmCompanyEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Could not connect to CRM to retrieve FRC companies " + e.getMessage());
		}
		
		for (FrcCrmCompanyEntity entity : entityList) {
			map.put(entity.getCompanyId(), entity.getCompanyName());
		}

		return map;
    	
    }

	public List<UserProfileDto> searchForCompaniesLike(String companyName) {
		List<UserProfileDto> returnList = new ArrayList<UserProfileDto>();
		Query query = frcEm.createQuery(JPQL_GET_USER_PROFILE_LIST_FOR_COMPANY_NAME);
		query.setParameter("company", companyName + "%");
		
		List<FrcUserProfileEntity> entityList;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Could not find any User belonging to the company starting with " + companyName + ". Error is " + e.getMessage());
		}
		
		for (FrcUserProfileEntity entity : entityList) {
			UserProfileDto dto = convertEntityToDto(entity);
			returnList.add(dto);
		}

		return returnList;
	}
	
	public UserProfileDto getUserProfileForUserName(String userName) {
		FrcUserProfileEntity entity = frcEm.find(FrcUserProfileEntity.class, userName);
		UserProfileDto dto = convertEntityToDto(entity);
		return dto;
	}
	
	private UserProfileDto convertEntityToDto(FrcUserProfileEntity entity) {
		UserProfileDto dto = null;
		if (entity != null) {
			dto = new UserProfileDto();
			dto.setUserId(entity.getUserName());
			dto.setEmail(entity.getEmail());
			dto.setCompanyId(entity.getCompanyId());
			dto.setCompanyName(entity.getCompanyName());
			boolean tarionAdmin = ldapSecurityBean.isMemberOf(entity.getUserName(), FrcConstants.LDAP_GROUP_TARION_ADMIN);
			if (tarionAdmin) {
				dto.setRoleType(RoleType.TARION_ADMIN);
			} else {
				dto.setRoleType(RoleType.FRC_USER);
			}
			dto.setUserFirstName(entity.getFirstName());
			dto.setUserLastName(entity.getLastName());
		}
		return dto;
	}
	
	private FrcUserProfileEntity convertDtoToEntity(UserProfileDto dto, String admin) {
		FrcUserProfileEntity entity = null;
		if (dto != null) {
			entity = new FrcUserProfileEntity();
			entity.setUserName(dto.getUserId());
			entity.setCompanyId(dto.getCompanyId());
			entity.setCompanyName(dto.getCompanyName());
			entity.setEmail(dto.getEmail());
			
			entity.setLastUpdateDt(FrcUtil.getDateNow());
			entity.setLastUpdateUserId(admin);
			
			entity.setFirstName(dto.getUserFirstName());
			entity.setLastName(dto.getUserLastName());
		}
		
		return entity;
	}
	
	public void createUser(UserProfileDto userProfileDto, String password, String admin) throws FrcUserDoesNotExistException {
		if (ldapSecurityBean.isUserIdInLdap(userProfileDto.getUserId())) {
			// User with that name already exists throw an error
			throw new FrcUserAlreadyExistsException("User with " + userProfileDto.getUserId() + " already exists");
		}
		FrcUserProfileEntity entity = convertDtoToEntity(userProfileDto, admin);
		entity.setCreateDate(FrcUtil.getDateNow());
		entity.setCreateUserId(admin);

		frcEm.persist(entity);
		frcEm.flush();
		
		// store new user in ldap
		ldapSecurityBean.addUser(userProfileDto.getUserId(), password, userProfileDto.isPasswordChangeRequired());
		String groupId = null;
		if (userProfileDto.getTarionAdminRole()) {
			groupId = FrcConstants.LDAP_GROUP_TARION_ADMIN;
		} else {
			groupId = FrcConstants.LDAP_GROUP_FRC_USER;
		}
		ldapSecurityBean.addUserToGroup(userProfileDto.getUserId(), groupId);
		sendRegistrationsSubmitEmail(userProfileDto, password);
	}
	
	private void sendRegistrationsSubmitEmail(UserProfileDto user, String password) {

		String emailBodyPattern = propSvc.getValue(FrcConstants.FRC_EMAIL_BODY_NEW_USER);
		String emailSubject = propSvc.getValue(FrcConstants.FRC_EMAIL_SUBJECT_NEW_USER);
		sendEmail(user.getUserId(), password, user.getEmail(), emailSubject, emailBodyPattern);
	}    
	
	private void sendEmail(String userId, String password, String email, String emailSubject, String emailBodyPattern) {
		String baseUrl = propSvc.getValue(FrcConstants.FRC_EMAIL_FRC_CONNECT_APP_URL);
				
		Object[] bodyArguments = { 
				userId, // {0} VB Name
				password,	// {1} Reference
				baseUrl}; // {2} FRC Base Url 

		MessageFormat formatter = new MessageFormat("");
		formatter.applyPattern(emailBodyPattern);
		String emailBody = formatter.format(bodyArguments);

		try {
			emailService.sendEmailUsingEsp(emailSubject, emailBody, "donotreply@tarion.com", email);
		} catch(Throwable t) {
			LoggerUtil.logError(UserProfileBean.class, "Error sending user creation email", t);
		}
	}

	public void updateUserEmailPassword(String userId, String email, boolean passwordChangeRequired, String password, String admin) throws FrcUserDoesNotExistException {
		FrcUserProfileEntity entity = frcEm.find(FrcUserProfileEntity.class, userId);
		if (entity == null || !ldapSecurityBean.isUserIdInLdap(userId)) {
			// User with that username does not exists
			throw new FrcUserDoesNotExistException("User with " + userId + " does not exist");
		}
		
		entity.setEmail(email);
		
		entity.setLastUpdateDt(FrcUtil.getDateNow());
		entity.setLastUpdateUserId(admin);
		
		frcEm.merge(entity);
		frcEm.flush();
		
		if (!FrcUtil.isNullOrEmpty(password)) {
			ldapSecurityBean.changePassword(userId, password, passwordChangeRequired);
			sendEmail(userId, password, email, propSvc.getValue(FrcConstants.FRC_EMAIL_SUBJECT_USER_PASSWORD_CHANGED), propSvc.getValue(FrcConstants.FRC_EMAIL_BODY_USER_PASSWORD_CHANGED));
		}
	}

	public void deleteUser(String userId, String admin) throws FrcUserDoesNotExistException {
		FrcUserProfileEntity entity = frcEm.find(FrcUserProfileEntity.class, userId);
		if (entity == null || !ldapSecurityBean.isUserIdInLdap(userId)) {
			// User with that name already exists throw an error
			throw new FrcUserDoesNotExistException("User with " + userId + " does not exist");
		}
		
		// remove user from group
		ldapSecurityBean.removeUserFromAllGroups(userId);
		
		// delete user from database
		frcEm.remove(entity);
		frcEm.flush();
		
		// remove user from LDAP
		ldapSecurityBean.deleteUser(userId);

	}

}
