/**
 * 
 */
package com.tarion.frc.userprofile;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;
import javax.naming.AuthenticationException;
import javax.naming.NamingException;

import com.tarion.frc.exception.FrcLdapException;
import com.tarion.frc.userprofile.dto.UserProfileDto;

/**
 * @author Bojan Volcansek
 *
 */
@Remote
public interface LdapSecurityBeanRemote {
    public boolean isPasswordChangeRequired(String userId) throws FrcLdapException;
	public List<UserProfileDto> searchLdapForUsersLike(String userId);
    public void changePassword(String userId, String password, boolean forceChangePassword) throws FrcLdapException;

    public boolean isMemberOf(String userId, String groupId) throws FrcLdapException;
    
    public Map<String, String> getAllRoles();
    public Map<String, String> getAllPermissions();    
    public Map<String, String> getAllPermissionsForRole(String roleId);
    
	/**
	 * Authenticate user with given password against LDAP server and return true
	 * if successful.
	 *
	 * @param userId
	 *            the user id
	 * @param password
	 *            the password
	 * @return true, if user is successfully authenticated
	 */
	public boolean isAllowed(String userId, String password) throws NamingException, AuthenticationException;
	public void addUser(String userName, String password, boolean passwordMustChange);
	public void addUserToGroup(String userId, String groupId) throws FrcLdapException;
	public void removeUserFromGroup(String userId, String groupId) throws FrcLdapException;
	public void removeUserFromAllGroups(String userId);
//	public void updateUserPermissions(UserProfileDto userProfile, String updatingUserId) throws FrcConnectException;
	public boolean isUserIdInLdap(String userId);
    public void deleteUser(String userId) throws FrcLdapException;

    public String getRandomPassword();

}
