/* 
 * 
 * UserProfileBeanRemote.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.userprofile;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import com.tarion.frc.exception.FrcUserAlreadyExistsException;
import com.tarion.frc.exception.FrcUserDoesNotExistException;
import com.tarion.frc.userprofile.dto.UserProfileDto;

/**
 * User Profile Bean.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
@Remote
public interface UserProfileBeanRemote {
    public Map<String, String> getAllCrmCompanies();
	public List<UserProfileDto> searchForCompaniesLike(String companyName);
	public UserProfileDto getUserProfileForUserName(String userName);
	public void createUser(UserProfileDto userProfileDto, String password, String admin)  throws FrcUserAlreadyExistsException;
	public void updateUserEmailPassword(String userId, String email, boolean passwordChangeRequired, String password, String admin) throws FrcUserDoesNotExistException;
	public void deleteUser(String userId, String admin)  throws FrcUserDoesNotExistException;
}
