/**
 * 
 */
package com.tarion.frc.userprofile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang3.RandomStringUtils;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.common.constants.FrcLdapConstants;
import com.tarion.frc.exception.FrcLdapException;
import com.tarion.frc.service.FrcProperties;
import com.tarion.frc.userprofile.dto.UserProfileDto;
import com.tarion.frc.userprofile.dto.UserProfileDto.RoleType;
import com.tarion.frc.util.LoggerUtil;

/**
 * @author Bojan Volcansek
 * 
 */
@Stateless(mappedName = "ejb/LdapSecurityBeanRemote")
@LocalBean
@PermitAll
public class LdapSecurityBean implements LdapSecurityBeanRemote {

	private DirContext context;
	
	@EJB
	private FrcProperties propSvc;

	/**
	 * This constructor constructs the LDAP security service using properties
	 * from the database.
	 */

	public LdapSecurityBean() {

		InitialContext initialContext;
		try {
			initialContext = new InitialContext();
			this.context = (DirContext) initialContext.lookup(FrcConstants.LDAP_CONTEXT);

		} catch (NamingException e) {
			LoggerUtil.logError(LdapSecurityBean.class, "Error getting LDAP Context: ", e);
			throw new FrcLdapException("Error getting LDAP Context: ", e);
		}

	}
	
	/**
     * This method will add a user to the LDAP.
     *
     * @param userName The username chosen
     * @param password the password
     * @param passwordMustChange the password must change
     * @throws FrcLdapException the bsa exception
     */
    public void addUser(String userName, String password, boolean passwordMustChange)
            throws FrcLdapException {
        try {
            // Create a container set of attributes
            Attributes container = new BasicAttributes();

            // Create the objectclass to add
            Attribute objClasses = new BasicAttribute("objectClass");
            objClasses.add("top");
            objClasses.add("person");
            objClasses.add("organizationalPerson");
            objClasses.add("inetOrgPerson");
            objClasses.add("pwdPolicy");
            objClasses.add("passwordPolicy");

            // Add these to the container
            container.put("uid", userName);
            container.put("userPassword", password);
            container.put("sn", userName);
            container.put("cn", userName);
            container.put("pwdMustChange", passwordMustChange ? "TRUE" : "FALSE");
            container.put("pwdAttribute", "userPassword");
            container.put(objClasses);

            // Create the entry

            String newUserName = "uid=" + userName + ",ou=users";
            context.bind(newUserName, context, container);
        } catch (NamingException ne) {
            String errMsg = "Error trying to add a new user";
            LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
            throw new FrcLdapException(errMsg, ne);
        }

        return;
    }
    
//    /**
//     * This method will reset a users password.
//     *
//     * @param userId the user id
//     * @return The new password
//     * @throws BsaException the bsa exception
//     */
//    public String resetPassword(String userId) throws FrcLdapException {
//        String password = null;
//        try {
//            password = getRandomPassword();
//            ModificationItem[] mods = new ModificationItem[2];
//            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(
//                    "userPassword", password.getBytes()));
//            mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(
//                    "pwdMustChange", "TRUE"));
//            context.modifyAttributes("uid=" + userId + ",ou=users", mods);
//        } catch (NamingException ne) {
//            String errMsg = "Error trying to reset a password";
//            LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
//            throw new FrcLdapException(errMsg, ne);
//        }
//        return password;
//    }

    /**
     * Randomly generate a new password.
     *
     * @return A randomly generated password
     */
    public String getRandomPassword() {
        return RandomStringUtils.randomAlphanumeric(8);
    }

    
    /**
     * Adds the user to a group.
     *
     * @param userId The users ID
     * @param groupId The group ID
     * @throws FrcLdapException the bsa exception
     */
    public void addUserToGroup(String userId, String groupId) throws FrcLdapException {

        // add to the required LDAP role
        try {
            ModificationItem[] roleMods = new ModificationItem[] { new ModificationItem(
                    DirContext.ADD_ATTRIBUTE, new BasicAttribute("uniqueMember",
                            getFullUserDN(userId))) };
            context.modifyAttributes(getFullGroupDN(groupId), roleMods);
        } catch (NamingException ne) {
            String errMsg = "Error trying to add a user to a group";
            LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
            throw new FrcLdapException(errMsg, ne);
        }

    }
    
    /**
     * Removes a user from a group.
     *
     * @param userId The user to remove
     * @param groupId The group to remove from
     * @throws FrcLdapException the bsa exception
     */
    public void removeUserFromGroup(String userId, String groupId) throws FrcLdapException {
        // add to the required LDAP role
        try {
            ModificationItem[] roleMods = new ModificationItem[] { new ModificationItem(
                    DirContext.REMOVE_ATTRIBUTE, new BasicAttribute("uniqueMember",
                            getFullUserDN(userId))) };
            context.modifyAttributes(getFullGroupDN(groupId), roleMods);
        } catch (NamingException ne) {
            String errMsg = "Error trying to remove a user from a group";
            LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
            throw new FrcLdapException(errMsg, ne);
        }
    }
    
    public void deleteUser(String userId) throws FrcLdapException {
		String userDN = getFullUserDN(userId);
		
		try {
			context.unbind(userDN);
		} catch (NamingException ne) {
            String errMsg = "Error trying to delete user: " + userId;
            LoggerUtil.logError(LdapSecurityBean.class, "Error Deleting User: " + userId, ne);
            throw new FrcLdapException(errMsg, ne);
		}	
    }
    
    public void removeUserFromAllGroups(String userId){
    	Map<String, String> groups = getAllPermissions();
    	for(Map.Entry<String, String> entry : groups.entrySet()){
    		try{
    			if(isMemberOf(userId, entry.getKey())){
    				removeUserFromGroup(userId, entry.getKey());
    			}
    		}catch (Exception e){
    			LoggerUtil.logError(LdapSecurityBean.class, "Unable to remove user: " + userId + " from group: " + entry.getKey(), e);
    		}
    	}
    }
    
    /**
     * 
     * Updates a user's permissions from the map supplied in the UserProfileDto.
     * 
     * @param userProfile The updated UserProfileDto
     * @param updatingUserId The UserId of the user doing the update.
     * @throws FrcConnectException the FRC Connect exception
     */
//    public void updateUserPermissions(UserProfileDto userProfile, String updatingUserId) throws FrcConnectException {
//    	
//    	for(Map.Entry<String, Boolean> entry : userProfile.getUserPermissionsMap().entrySet()){
//    		String permission = entry.getKey();
//    		Boolean setting = entry.getValue();
//    		
//    		if(isMemberOf(userProfile.getUserId(), permission) != setting){
//    			//changing
//    			if(setting){
//    				addUserToGroup(userProfile.getUserId(), permission);
//    			}else{
//    				removeUserFromGroup(userProfile.getUserId(), permission);
//    			}
//    			
//    			String change;
//    			if(setting){change = "ADDED";}else{change = "REMOVED";}
//    		}
//    	}
//    	
//    }
    
    /**
     * Gets the full user dn.
     *
     * @param userId the user id
     * @return the full user dn
     */
    private String getFullUserDN(String userId) {
        return "uid=" + userId + ",ou=users";
    }

    /**
     * Gets the full group dn.
     *
     * @param groupId the group id
     * @return the full group dn
     */
    private String getFullGroupDN(String groupId) {
        return "cn=" + groupId + ",ou=groups";// ,dc=bsa,dc=tarion,dc=com";
    }
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tarion.bsa.userprofile.LdapSecurityBeanRemote#isMemberOf(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public boolean isMemberOf(String userId, String groupId) throws FrcLdapException {
		try {
			String[] d = { "uniqueMember" };
			Attributes matchAttrs = context.getAttributes("cn=" + groupId + ",ou=groups", d);
			NamingEnumeration<?> answer = matchAttrs.getAll();
			BasicAttribute attr;
			while (answer.hasMore()) {
				attr = (BasicAttribute) answer.next();
				NamingEnumeration<?> values = attr.getAll();
				String value;
				while (values.hasMore()) {
					value = (String) values.next();
					String[] splitValues = value.split(",");
					for (String string : splitValues) {
						if (string.startsWith("uid=")) {
							String formattedUserId = "uid=" + userId;
							if (formattedUserId.equals(string)) {
								return true;
							}
						}
					}
				}
			}
		} catch (NamingException ne) {
			String errMsg = "Error trying to check isMember";
			LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
			throw new FrcLdapException(errMsg, ne);
		}
		return false;
	}
	
    /**
     * A search in LDAP for the user logging in. A check is done on the
     * "pwdMustChange" attribute to determine if they are required to change
     * their password.
     *
     * @param userId the user id
     * @return True if the user is required to changed there password.
     * @throws FrcLdapException the FRC LDAP exception
     */
    public boolean isPasswordChangeRequired(String userId) throws FrcLdapException {
        try {
            String[] d = { "pwdMustChange" };
            StringBuilder searchString = new StringBuilder();
            searchString.append("uid=").append(userId).append(",ou=users");

            Attributes matchAttrs = context.getAttributes(searchString.toString(), d);
            NamingEnumeration<? extends Attribute> answer = matchAttrs.getAll();

            BasicAttribute attr;
            while (answer.hasMore()) {
                attr = (BasicAttribute) answer.next();
                NamingEnumeration<?> values = attr.getAll();
                String value;
                while (values.hasMore()) {
                    value = (String) values.next();
                    if (null != value && value.equals("TRUE")) {
                        return true;
                    }
                }
            }
        } catch (NamingException ne) {
            throw new FrcLdapException("Exception occurred during lookup of user info", ne);
        }
        return false;
    }


	@Override
	public Map<String, String> getAllRoles() {
		Map<String, String> roles = new HashMap<String, String>();
		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

			NamingEnumeration<SearchResult> answers = context.search("ou=roles", "objectClass=groupOfUniqueNames", controls);
			BasicAttribute attr;
			while (answers.hasMore()) {
				SearchResult answer = answers.next();
				Attributes attributes = answer.getAttributes();
				NamingEnumeration<?> allAttributes = attributes.getAll();
				String roleName = null;
				String roleDescription = null;
				while (allAttributes.hasMore()) {
					attr = (BasicAttribute) allAttributes.next();
					if (attr.getID().equalsIgnoreCase("cn")) {
						roleName = (String) attr.getAll().next();
					} else if (attr.getID().equalsIgnoreCase("description")) {
						roleDescription = (String) attr.getAll().next();
					}
				}
				if (roleName != null) {
					roles.put(roleName, roleDescription);
				}
			}
		} catch (NamingException ne) {
			String errMsg = "Error trying to check isMember";
			LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
			throw new FrcLdapException(errMsg, ne);
		}

		return roles;
	}
	
	@Override
	public List<UserProfileDto> searchLdapForUsersLike(String userId) {
		List<UserProfileDto> returnList = new ArrayList<UserProfileDto>();
		String baseDn = "ou=users";
		String searchFilter = "uid=" + userId + "*";

		try {

			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

			NamingEnumeration<?> answer;
//			NamingEnumeration<SearchResult> answers = context.search("ou=users", "objectClass=groupOfUniqueNames", controls);
			answer = context.search(baseDn, searchFilter, controls);
			
			while (answer.hasMore()) {
				UserProfileDto dto = new UserProfileDto();
				
				SearchResult sr = (SearchResult) answer.next();
				Attributes attributes = sr.getAttributes();
				fillUserDto(dto, attributes);
				
				returnList.add(dto);
			}
			
		} catch (NamingException e) {
			String msg = "Failed to get user profile: " + searchFilter;
			LoggerUtil.logError(LdapSecurityBean.class, msg, e);
		}

		return returnList;
	}
	
	private void fillUserDto(UserProfileDto dto, Attributes attributes)
			throws NamingException {
		NamingEnumeration<?> ae = attributes.getAll();
		while (ae.hasMore()) {
			
			Attribute attr = (Attribute) ae.next();
			if (attr.getID().equals(FrcLdapConstants.USER_ID)) {
				dto.setUserId((String)attr.getAll().next());
			}
		}
		if (isMemberOf(dto.getUserId(), FrcConstants.LDAP_GROUP_TARION_ADMIN)) {
			dto.setRoleType(RoleType.TARION_ADMIN);
		} else {
			dto.setRoleType(RoleType.FRC_USER);
		}
	}
	
	@Override
	public boolean isUserIdInLdap(String userId) {
		boolean userExistsInLdap = false;
		try {
			String[] d = { "person" };
			context.getAttributes("uid=" + userId + ",ou=users", d);
			userExistsInLdap = true;
		} catch (NamingException ne) {
			userExistsInLdap = false;
		}

		return userExistsInLdap;
	}
	

	@Override
	public Map<String, String> getAllPermissions() {
		Map<String, String> roles = new HashMap<String, String>();
		try {
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);

			NamingEnumeration<SearchResult> answers = context.search("ou=groups", "objectClass=groupOfUniqueNames", controls);
			BasicAttribute attr;
			while (answers.hasMore()) {
				SearchResult answer = answers.next();
				Attributes attributes = answer.getAttributes();
				NamingEnumeration<?> allAttributes = attributes.getAll();
				String permissionName = null;
				String permissionDescription = null;
				while (allAttributes.hasMore()) {
					attr = (BasicAttribute) allAttributes.next();
					if (attr.getID().equalsIgnoreCase("cn")) {
						permissionName = (String) attr.getAll().next();
					} else if (attr.getID().equalsIgnoreCase("description")) {
						permissionDescription = (String) attr.getAll().next();
					}
				}
				if (permissionName != null) {
					roles.put(permissionName, permissionDescription);
				}
			}
		} catch (NamingException ne) {
			String errMsg = "Error trying to check isMember";
			LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
			throw new FrcLdapException(errMsg, ne);
		}

		return roles;
	}

	@Override
	public Map<String, String> getAllPermissionsForRole(String roleId) {
		Map<String, String> permissions = new HashMap<String, String>();
		List<String> allPermissionKeysForRole = new ArrayList<String>();
		String ldapAttributeDescription = "description";
		String ldapAttributeUniqueMember = "uniqueMember";
		try {
			String[] d = { ldapAttributeUniqueMember };
			Attributes matchAttrs = context.getAttributes("cn=" + roleId + ",ou=roles", d);
			NamingEnumeration<?> answer = matchAttrs.getAll();
			BasicAttribute attr;
			while (answer.hasMore()) {
				attr = (BasicAttribute) answer.next();
				NamingEnumeration<?> values = attr.getAll();
				String value;
				while (values.hasMore()) {
					value = (String) values.next();
					String[] splitValues = value.split(",");
					if (splitValues[0].startsWith("uid=")) {
						allPermissionKeysForRole.add(splitValues[0].substring(4));
					}

				}
			}
		} catch (NamingException ne) {
			String errMsg = "Error trying to get all permissions for passed role";
			LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
			throw new FrcLdapException(errMsg, ne);
		}

		for (String permissionKey : allPermissionKeysForRole) {
			try {
				String[] d = { ldapAttributeDescription };
				Attributes matchAttrs = context.getAttributes("cn=" + permissionKey + ",ou=groups", d);
				NamingEnumeration<?> answer = matchAttrs.getAll();
				BasicAttribute attr;
				while (answer.hasMore()) {
					attr = (BasicAttribute) answer.next();
					NamingEnumeration<?> values = attr.getAll();
					String permissionDescription;
					while (values.hasMore()) {
						permissionDescription = (String) values.next();
						permissions.put(permissionKey, permissionDescription);
					}
				}
			} catch (NamingException e) {
				String errMsg = "Error trying to get description for passed permission";
				LoggerUtil.logError(LdapSecurityBean.class, errMsg, e);
				throw new FrcLdapException(errMsg, e);
			}
		}
		return permissions;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tarion.bsa.userprofile.LdapSecurityBeanRemote#isAllowed(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public boolean isAllowed(final String userId, final String password) throws AuthenticationException, NamingException {

		boolean result = false;

		String base = propSvc.getValue(FrcConstants.FRC_LDAP_BASE);
		String dn = "uid=" + userId + "," + base;
		
		Hashtable<?, ?> localContext = this.context.getEnvironment();
		
		// Setup environment for authenticating
		Hashtable<String, String> environment = new Hashtable<String, String>();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, (String)localContext.get(Context.INITIAL_CONTEXT_FACTORY));
		environment.put(Context.PROVIDER_URL, (String)localContext.get(Context.PROVIDER_URL));
		environment.put(Context.SECURITY_AUTHENTICATION, (String)localContext.get(Context.SECURITY_AUTHENTICATION));
		environment.put(Context.SECURITY_PRINCIPAL, dn);
		environment.put(Context.SECURITY_CREDENTIALS, password);

		try {
			new InitialDirContext(environment);
			// user is authenticated if no exception occured
			result = true;
		} catch (AuthenticationException ex) {
			LoggerUtil.logInfo(LdapSecurityBean.class, "Authentication Credentials invalid" + ex);
			throw ex;
		} catch (NamingException ex) {
			LoggerUtil.logError(LdapSecurityBean.class, "Naming exception occured" + ex);
			throw ex;
		}

		return result;
	}
	
	@Override
    public void changePassword(String userId, String password, boolean forceChangePassword) throws FrcLdapException {
        try {
            ModificationItem[] mods = new ModificationItem[2];
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(
                    "userPassword", password.getBytes()));
            mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(
                    "pwdMustChange", forceChangePassword ? "TRUE" : "FALSE"));
            context.modifyAttributes("uid=" + userId + ",ou=users", mods);
        } catch (NamingException ne) {
            String errMsg = "Error trying to change a password";
            LoggerUtil.logError(LdapSecurityBean.class, errMsg, ne);
            throw new FrcLdapException(errMsg, ne);
        }
    }

}
