/* 
 * 
 * ContentBeanRemote.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.content;

import javax.ejb.Remote;

/**
 * Content Bean.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-03-28
 * @version 1.0
 */
@Remote
public interface ContentBeanRemote {
	public void updateOutageMessage(String outageMessage);
	public String getOutageMessage();
}
