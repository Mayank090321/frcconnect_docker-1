/* 
 * 
 * UserProfileBean.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.content;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tarion.frc.content.dto.ContentPageType;
import com.tarion.frc.content.dto.ContentTopicType;
import com.tarion.frc.entity.FrcContentEntity;
import com.tarion.frc.entity.FrcCrmCompanyEntity;
import com.tarion.frc.exception.FrcDatabaseException;
import com.tarion.frc.service.FrcProperties;
import com.tarion.frc.util.FrcUtil;

/**
 * User Profile Bean.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
@Stateless(mappedName = "ejb/ContentBeanRemote")
@LocalBean
public class ContentBean implements ContentBeanRemote {
	private static final String JPQL_GET_OUTAGE_ENTITY =
			" select u from FrcContentEntity u " +
			" where UPPER(u.pageType) = UPPER('" + ContentPageType.HOME_PAGE + "')" +
			" and UPPER(u.topicType) = UPPER('" + ContentTopicType.OUTAGE_MESSAGE + "') ";

	@EJB
	private FrcProperties propSvc;
    
    /** FRC Persistence Context */
    @PersistenceContext(unitName = "frc")
    private EntityManager frcEm;

    /**
     * Default constructor.
     */
    public ContentBean() {
    }
    
	public void updateOutageMessage(String outageMessage) {
		FrcContentEntity entity = getOutageEntity();
		entity.setContent(outageMessage);
		entity.setPublishDate(FrcUtil.getDateNow());
		frcEm.merge(entity);
	}
	
	public String getOutageMessage() {
		FrcContentEntity entity = getOutageEntity();
		String outageMessage = entity.getContent();
		
		return outageMessage;
	}
	
	private FrcContentEntity getOutageEntity() {
		FrcContentEntity entity = null;
		Query query = frcEm.createQuery(JPQL_GET_OUTAGE_ENTITY);
		
		List<FrcContentEntity> entityList = null;
		try {
			entityList = query.getResultList();
		} catch (Exception e) {
			throw new FrcDatabaseException("Could not ftech outage message " + e.getMessage());
		}
		if (entityList != null && entityList.size() > 0) {
			entity = entityList.get(0);
		} else {
			entity = new FrcContentEntity();
			entity.setContent("");
			entity.setCreatedDate(FrcUtil.getDateNow());
			entity.setPublishDate(FrcUtil.getDateNow());
			entity.setPageType(ContentPageType.HOME_PAGE);
			entity.setTopicType(ContentTopicType.OUTAGE_MESSAGE);
			entity.setTopic(ContentTopicType.OUTAGE_MESSAGE.getName());
			frcEm.persist(entity);
		}
		
		return entity;
	}

}
