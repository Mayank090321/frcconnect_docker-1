/* 
 * 
 * JNDIServicesLocator.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.service.FrcProperties;
import com.tarion.frc.util.LoggerUtil;



/**
 * JndiServicesLocator is used to centralize all JNDI lookups. 
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @date 2016-02-15
 * @version 1.0
 */
public class JNDIServicesLocator {
	
    private InitialContext initialContext;
    private Map<String, Object> cache = Collections.synchronizedMap(new HashMap<String, Object>());

    /**
     * Clear the cache.
     */
    public synchronized void clear() {
        cache.clear();
    }

	/**
	 * Generic method that looks up the remote EJB based on the package and class name of given EJB.
	 * 
	 * 
	 */
	protected <T> T getBean(Class<T> clazz) {
		long startTime = LoggerUtil.logEnter(JNDIServicesLocator.class, "getBean", clazz);
		String simpleName = clazz.getSimpleName();
		String packageName = clazz.getPackage().getName();
		String jndiName = "ejb/" + simpleName + "#" + packageName + "." + simpleName;
		Object ret = getJndiService(jndiName); 
		LoggerUtil.logExit(JNDIServicesLocator.class, "getBean", ret, startTime);
		return (T)ret;
	}

    /**
     * Get the JNDI Service for the specified JNDI name.
     *
     * @param jndiName The name to lookup
     * @return The Context of the specified name
     */
    private Object getJndiService(String jndiName) {

        Object businessService = cache.get(jndiName);

        if (businessService == null && !cache.containsKey(jndiName)) {
            try {
                businessService = lookup(jndiName);
                cache.put(jndiName, businessService);
            } catch (RuntimeException e) {
                clear();
                throw e;
            }
        }
        return businessService;
    }

    /**
     * Lookup the JNDI name.
     *
     * @param name the JNDI name to retrieve
     * @return The Initial Context
     */
    private synchronized Object lookup(String name) {

        // Recheck the cache because the name we're looking for may have been
        // added while we were waiting for sync.

        if (!cache.containsKey(name)) {
            try {
                return getInitialContext().lookup(name);
            } catch (NameNotFoundException e) {
                clear();
            } catch (NamingException e) {
                clear();
            }
        } else {
            return cache.get(name);
        }
        return null;
    }

    /**
     * Get the Initial Context.
     *
     * @return The Intitial Context
     */
    private synchronized InitialContext getInitialContext() {
		if (initialContext == null){
			
			Properties props = new Properties();
			props.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");			
			props.put("java.naming.provider.url", System.getProperty("frcJNDIUrl"));
			// putting property in system YAML file and not in DB
//			props.put("java.naming.provider.url", getPropertyService().getValue(FrcConstants.LDAP_WEBLOGIC_EJB_URL));
			try {
				initialContext = new InitialContext(props);
//TODO do not commit
//				initialContext = new InitialContext();
			} catch (NamingException e) {
				clear();
				throw new RuntimeException(e);
			}
		}

		return initialContext;
    }

	// This is local EJB, not on the TIP server
	private FrcProperties getPropertyService() {

		String simpleName = FrcProperties.class.getSimpleName();
		String packageName = FrcProperties.class.getPackage().getName();
		String jndiName = "ejb/" + simpleName + "#" + packageName + "." + simpleName;
		
		try {
			InitialContext ic = new InitialContext();
			return (FrcProperties) ic.lookup(jndiName);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}


}
