/* 
 * 
 * RequestUtil.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.util;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.util.ELRequestMatcher;
import org.springframework.security.web.util.RequestMatcher;

import com.tarion.frc.util.LoggerUtil;

/**
 * Request Util
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-25
 * @version 1.0
 */
public class RequestUtil {
	 
	private static final RequestMatcher REQUEST_MATCHER = new ELRequestMatcher("hasHeader('X-Requested-With','XMLHttpRequest')");
	public static final String JSON_VALUE = "{\"%s\": %s}";
	
	
	public static Boolean isAjaxRequest(HttpServletRequest request) {
		return REQUEST_MATCHER.matches(request);
	}
	
	public static void sendJsonResponse(HttpServletResponse response, String responseJson) {
		response.setContentType("application/json;charset=UTF-8");           
        response.setHeader("Cache-Control", "no-cache");
        try {
			response.getWriter().write(responseJson);
		} catch (IOException e) {
			LoggerUtil.logError(RequestUtil.class, "error writing json to response", e);
		}
	}
	

	
}
