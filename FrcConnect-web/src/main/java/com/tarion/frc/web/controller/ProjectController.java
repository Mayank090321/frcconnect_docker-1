/* 
 * 
 * ProjectController.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.controller;

import java.io.IOException;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.common.dto.DocumentUploadDto;
import com.tarion.frc.common.dto.NameValueDto;
import com.tarion.frc.project.dto.PreviousDocumentsDto;
import com.tarion.frc.project.dto.ProjectDto;
import com.tarion.frc.userprofile.dto.UserProfileDto;
import com.tarion.frc.util.FrcUtil;
import com.tarion.frc.util.LoggerUtil;
import com.tarion.frc.web.util.BusinessServicesLocator;
import com.tarion.tbi.cm.attachments.ReferenceInfo;
import com.tarion.tbi.cm.attachments.SubmissionResponse;
import com.tarion.tbi.services.FrcEnrolmentInfoResponse;

/**
 * Project Controller.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-25
 * @version 1.0
 */
@RequestMapping("/project")
@Controller
@Scope("session") 
public class ProjectController {

	@Autowired
	private BusinessServicesLocator businessServiceLocator;
	
	private List<NameValueDto> allTemplateTypeList = null;
	
	private int reportNumber = 0;
	private String enrolment = null;
	private String vb = null;
	private String outageMessage = null;
	
	private List<DocumentUploadDto> documentUpoadList = null;
	List<ProjectDto> searchProjectResultList = null;

	@RequestMapping(value="/monitor", method={RequestMethod.GET, RequestMethod.HEAD})
	public @ResponseBody String monitor(){
		return "FRC STATUS OK";
	}

	
	@RequestMapping(value = "/searchForProjectsLike", method = RequestMethod.POST)
	public String searchForProjectsLike(@RequestParam(required=false) String streetNumber, @RequestParam(required=false) String streetName, 
			@RequestParam(required=false) String projectName, @RequestParam(required=false) String projectEnrolment, 
			Principal principal, Model uiModel) {
		UserProfileDto userProfile = businessServiceLocator.getUserProfileRemote().getUserProfileForUserName(principal.getName());
		if (!FrcUtil.isNullOrEmpty(streetNumber) || !FrcUtil.isNullOrEmpty(streetName)) {
			searchProjectResultList = businessServiceLocator.getProjectBeanRemote().searchForProjectsLikeAddress(userProfile.getCompanyId(), streetNumber, streetName);
		} else if (!FrcUtil.isNullOrEmpty(projectName)) {
			searchProjectResultList = businessServiceLocator.getProjectBeanRemote().searchForProjectsLikeProjectName(userProfile.getCompanyId(), projectName);
		}else if (!FrcUtil.isNullOrEmpty(projectEnrolment)) {
			projectEnrolment = projectEnrolment.trim();
			if(!projectEnrolment.toUpperCase().startsWith("H")){
				projectEnrolment = "H" + projectEnrolment;
			}
			searchProjectResultList = businessServiceLocator.getProjectBeanRemote().searchForProjectsLikeEnrolmentId(projectEnrolment);
			
		}
		
		uiModel.addAttribute("projectList", searchProjectResultList);
		if (!FrcUtil.isNullOrEmpty(outageMessage)) {
			uiModel.addAttribute("outageMessage", outageMessage);
		}
		return "searchProjectView";
	}
	
	// todo remove this - it is test method for json table pagination
//	@RequestMapping(value = "/searchForProjectsLikeProjectName", method = RequestMethod.GET, produces = "application/json")
//	public @ResponseBody List<ProjectDto> searchForProjectsLike(@RequestParam(required=false) String projectName) {
//		if (FrcUtil.isNullOrEmpty(projectName)) {
//			projectName = "the";
//		}
//		String testCompanyId = "12234300";
//		List<ProjectDto> returnList = businessServiceLocator.getProjectBeanRemote().searchForProjectsLikeProjectName(testCompanyId, projectName);
//		
//		return returnList;
//	}
	
	@RequestMapping(value = "/frcUserLogin")
	public String frcUserLogin(Model uiModel)	{
		// this init is not needed since
		// @Scope("session") 
		// is added
		enrolment = null;
		vb = null;
		reportNumber = 0;
		allTemplateTypeList = null;
		documentUpoadList = null;
		searchProjectResultList = null;
		outageMessage = businessServiceLocator.getContentBeanRemote().getOutageMessage();
		uiModel.addAttribute("projectList", searchProjectResultList);
		if (!FrcUtil.isNullOrEmpty(outageMessage)) {
			uiModel.addAttribute("outageMessage", outageMessage);
		}
		return "searchProjectView";
	}
	

	@RequestMapping(value = "/searchProject")
	public String initSearchProject(Model uiModel)	{
		enrolment = null;
		vb = null;
		reportNumber = 0;
		allTemplateTypeList = null;
		documentUpoadList = null;
		//searchProjectResultList = null;
		outageMessage = businessServiceLocator.getContentBeanRemote().getOutageMessage();
		uiModel.addAttribute("projectList", searchProjectResultList);
		if (!FrcUtil.isNullOrEmpty(outageMessage)) {
			uiModel.addAttribute("outageMessage", outageMessage);
		}
		return "searchProjectView";
	}
	
	@RequestMapping(value = "/uploadPage")
	public String uploadPage(@RequestParam String enrolmentNumber, @RequestParam String vbNumber, Model uiModel) {
		FrcEnrolmentInfoResponse enrolmentInfo = businessServiceLocator.getProjectBeanRemote().getEnrolmentInfo(enrolmentNumber);
		if (reportNumber == 0) {
			reportNumber = businessServiceLocator.getProjectBeanRemote().getCurrentSixtyDayReportNumberForEnrolment(enrolmentNumber);
		}
		reportNumber++;
		enrolment = enrolmentNumber;
		vb = vbNumber;
		
		String streetAddress = enrolmentInfo.getStreetAddress();
		String suiteNumber = enrolmentInfo.getSuiteNumber();
		String city = enrolmentInfo.getCity();
		String province = enrolmentInfo.getProvince();
		String postalCode = enrolmentInfo.getPostalCode();
		
		String wsrName = enrolmentInfo.getWsrName();
		String wsrEmail = enrolmentInfo.getWsrEmail();
		
		
		uiModel.addAttribute("enrolmentNumber", enrolmentNumber);
		if (allTemplateTypeList == null) {
			allTemplateTypeList = businessServiceLocator.getProjectBeanRemote().getAllB19TemplateTypes();
		}
		uiModel.addAttribute("allTemplateTypeList", allTemplateTypeList);
		uiModel.addAttribute("reportNumber", reportNumber);
		uiModel.addAttribute("sixtyDayReportCode", FrcConstants.FRC_CM_TEMPLATE_TYPE_CODE_60_DAY_TEMPLATE);
		uiModel.addAttribute("documentUpoadList", documentUpoadList);
		
		
		uiModel.addAttribute("streetAddress", streetAddress);
		uiModel.addAttribute("suiteNum", suiteNumber);
		uiModel.addAttribute("city", city);
		uiModel.addAttribute("province", province);
		uiModel.addAttribute("postalCode", postalCode);
		uiModel.addAttribute("wsrName", wsrName);
		uiModel.addAttribute("wsrEmail", wsrEmail);
		
		List<PreviousDocumentsDto> previousDocumentsList = businessServiceLocator.getProjectBeanRemote().getPreviousDocuments(enrolmentNumber);
		
		uiModel.addAttribute("previousDocumentsList", previousDocumentsList);

		//uiModel.addAttribute("projectList", searchProjectResultList);
		return "uploadDocumentsView";
	}
	
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public String uploadFile(@RequestParam MultipartFile uploadedFile, @RequestParam String templateTypeSelect, 
			@RequestParam(required=false) String startDate, @RequestParam(required=false) String endDate, 
			@RequestParam(required=false) String reportNumberUpdate, Model uiModel) {
		if (documentUpoadList == null) {
			documentUpoadList = new ArrayList<DocumentUploadDto>();
		}
		DocumentUploadDto dto = new DocumentUploadDto();
		NameValueDto templateTypeDto = null;
		for (NameValueDto nameValueDto : allTemplateTypeList) {
			if (nameValueDto.getName().equalsIgnoreCase(templateTypeSelect)) {
				templateTypeDto = nameValueDto;
				break;
			}
		}
		dto.setTemplateType(templateTypeDto);
		if (!FrcUtil.isNullOrEmpty(startDate)) {
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
			try {
				dto.setStartDate(dateFormatter.parse(startDate));
				dto.setEndDate(dateFormatter.parse(endDate));
			} catch (ParseException e) {
				// this should never happened since validation is on the IO
				LoggerUtil.logError(ProjectController.class, "Error converting StartDate / EndDate to string", e);
			}
			dto.setReportNumber("" + reportNumberUpdate);
		} else {
			reportNumber--;
		}
		dto.setFilename(uploadedFile.getOriginalFilename());
		//dto.setMimeFileType(uploadedFile.);
		try {
			dto.setFileData(uploadedFile.getBytes());
		} catch (IOException e) {
			LoggerUtil.logError(ProjectController.class, "Error getting the file byte array", e);
		}
		documentUpoadList.add(dto);
		prepareReloadUpload(uiModel);
		return "redirect:uploadPage";
	}
	
	@RequestMapping(value = "/uploadAllFiles", method = RequestMethod.POST)
	public String uploadAllFiles(Principal principal, Model uiModel) {
		
		SubmissionResponse submissionResponse = null;
		String uploadStatus = null;
		try {
			submissionResponse = businessServiceLocator.getProjectBeanRemote().uploadFilesToCmAndSendCrmWorklist(principal.getName(), vb, enrolment, documentUpoadList);
			StringBuilder errMsg = null;
			for (ReferenceInfo refInfo : submissionResponse.getReferenceInfoList().getReferenceInfo()) {
				if (refInfo.getStatus().equalsIgnoreCase("FAILURE")) {
					if (errMsg == null) {
						errMsg = new StringBuilder();
					}
					errMsg.append(errMsg.length() > 0 ? ", " : "");
					errMsg.append(refInfo.getErrorMsg());
				}
			}
			uploadStatus = errMsg == null ? null : errMsg.toString();
			if (uploadStatus != null) {
				LoggerUtil.logError(ProjectController.class, "Error occured while upload documents to CM " + uploadStatus);
			}
		} catch (Exception e) {
			LoggerUtil.logError(ProjectController.class, "Error occured while upload documents to CM ", e);
			uploadStatus = e.getMessage();
		}
		uiModel.addAttribute("uploadStatus", uploadStatus);
		uiModel.addAttribute("enrolmentNumber", enrolment);
		uiModel.addAttribute("currentDateTime", FrcUtil.getDateNowFormatted());
		uiModel.addAttribute("vbNumber", vb);
		return "uploadFilesSuccessView";
	}
	
	@RequestMapping(value = "/uploadFilesSuccess")
	public String uploadFilesSuccess(Model uiModel) {
		return "redirect:searchProject";
	}
	
	
	@RequestMapping(value = "/cancelUpload", method = RequestMethod.POST)
	public String cancelUpload(Model uiModel) {
		return "redirect:searchProject";
	}
	
	@RequestMapping(value = "/removeFile")
	public String removeFile(@RequestParam String filename, Model uiModel) {
		int removedIndex = -1;
		for (int i = 0; i < documentUpoadList.size(); i++) {
			DocumentUploadDto dto = documentUpoadList.get(i);
			if (filename.equalsIgnoreCase(dto.getFilename())) {
				if (FrcConstants.FRC_CM_TEMPLATE_TYPE_CODE_60_DAY_TEMPLATE.equalsIgnoreCase(dto.getTemplateType().getName())) {
					reportNumber--;
				}
				removedIndex = i;
				documentUpoadList.remove(i);
				break;
			}
		}
		// resync other report numbers
		for (int i = 0; i < documentUpoadList.size(); i++) {
			DocumentUploadDto dto = documentUpoadList.get(i);
			if (i > removedIndex) {
				if (FrcConstants.FRC_CM_TEMPLATE_TYPE_CODE_60_DAY_TEMPLATE.equalsIgnoreCase(dto.getTemplateType().getName())) {
					dto.setReportNumber("" + (Integer.parseInt(dto.getReportNumber()) - 1));
				}
			}
		}
		prepareReloadUpload(uiModel);
		return "redirect:uploadPage";
	}

	
	private void prepareReloadUpload(Model uiModel) {
		uiModel.addAttribute("enrolmentNumber", enrolment);
		uiModel.addAttribute("vbNumber", vb);
	}

}