/* 
 * 
 * FrcWebException.java
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.exception;

/**
 * FrcWebException Exception Class.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2014-07-10
 * @version 1.0
 */
public class FrcWebException extends RuntimeException {
    
       /** Serial version ID */
       private static final long serialVersionUID = 1L;

       /** Error code */
       protected String errorCode = null;

       /**
        * Instantiates a new FrcWebException.
        */
       public FrcWebException() {
       }

       /**
        * Instantiates a new FrcWebException.
        *
        * @param msg the msg
        */
       public FrcWebException(String msg) {
           super(msg);
       }

       /**
        * Instantiates a new FrcWebException.
        *
        * @param msg the msg
        * @param errorCode the error code
        */
       public FrcWebException(String msg, String errorCode) {
           super(msg);
           this.errorCode = errorCode;
       }

       /**
        * Instantiates a new FrcWebException.
        *
        * @param msg the msg
        * @param errorCode the error code
        * @param cause the cause
        */
       public FrcWebException(String msg, String errorCode, Throwable cause) {
           super(msg, cause);
           this.setStackTrace(cause.getStackTrace());
           this.errorCode = errorCode;
       }

       /**
        * Instantiates a new FrcWebException.
        *
        * @param errorCode the error code
        * @param cause the cause
        */
       public FrcWebException(String errorCode, Throwable cause) {
           super(cause);
           this.setStackTrace(cause.getStackTrace());
           this.errorCode = errorCode;
       }

       /**
        * Gets the error code.
        *
        * @return the error code
        */
       public String getErrorCode() {
           return errorCode;
       }

       /**
        * Return the error code number, by first stripping off the beginning value
        * <code>DASH-</code>.
        *
        * @return Error code number (eg. BSA-99999, will return 99999)
        */
       public int getErrorCodeNumber() {
           int errorCodeNumber = 0;
           if (errorCode != null && errorCode.startsWith("DASH")) {
               java.util.StringTokenizer st = new java.util.StringTokenizer(errorCode, "DASH-");
               String code = st.nextToken();
               try {
                   errorCodeNumber = Integer.parseInt(code);
               } catch (NumberFormatException nfe) {
                   // Default to negative one if bad error code number
                   errorCodeNumber = -1;
               }
           }
           return errorCodeNumber;
       }

       /* (non-Javadoc)
        * @see java.lang.Throwable#toString()
        */
       @Override
       public String toString() {
           StringBuffer theString = new StringBuffer();
           if (errorCode != null) {
               theString.append(errorCode);
               theString.append(": ");
           }
           theString.append(super.toString());
           return theString.toString();
       }
}