/* 
 * 
 * BusinessServicesLocator.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.util;

import com.tarion.frc.content.ContentBeanRemote;
import com.tarion.frc.project.ProjectBeanRemote;
import com.tarion.frc.userprofile.LdapSecurityBeanRemote;
import com.tarion.frc.userprofile.UserProfileBeanRemote;

/**
 * The Interface BusinessServicesLocator.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
public interface BusinessServicesLocator {


	/**
	 * Gets the user profile remote.
	 *
	 * @return the user profile remote
	 */
	public abstract UserProfileBeanRemote getUserProfileRemote();
	

    /**
     * Gets the Ldap Security bean remote.
     *
     * @return the Ldap Security bean remote.
     */
    public LdapSecurityBeanRemote getLdapSecurityBeanRemote();
    
    /**
     * Gets the Project bean remote.
     *
     * @return the Project bean remote.
     */
    public ProjectBeanRemote getProjectBeanRemote();
    
	public ContentBeanRemote getContentBeanRemote();

}
