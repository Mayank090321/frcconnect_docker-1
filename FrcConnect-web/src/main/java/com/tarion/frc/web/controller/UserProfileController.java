/* 
 * 
 * UserProfileController.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.common.dto.NameValueDto;
import com.tarion.frc.userprofile.dto.UserProfileDto;
import com.tarion.frc.userprofile.dto.UserProfileDto.RoleType;
import com.tarion.frc.util.FrcUtil;
import com.tarion.frc.web.util.BusinessServicesLocator;

/**
 * User Profile Controller.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-25
 * @version 1.0
 */
@RequestMapping("/user")
@Controller
@Scope("session") 
public class UserProfileController {

	String currentOutageMessage = null;
	@Autowired
	private BusinessServicesLocator businessServiceLocator;
	
	private Map<String, String> crmCompanyMap = null;

	@RequestMapping(value = "/saveOutageMessage", method = RequestMethod.POST)
	public String saveOutageMessage(@RequestParam("outageMessage") String outageMessage, Model uiModel) {
		businessServiceLocator.getContentBeanRemote().updateOutageMessage(outageMessage);
		currentOutageMessage = outageMessage;
		uiModel.addAttribute("currentOutageMessage", currentOutageMessage);
		return "searchCompanyView";
	}

	@RequestMapping(value = "/getCompaniesLike", method = RequestMethod.GET)
	public @ResponseBody List<NameValueDto> getCompaniesLike(@RequestParam String companyNameLike) {
		List<NameValueDto> companyList = new ArrayList<NameValueDto>();

		if (!FrcUtil.isNullOrEmpty(companyNameLike)) {
			if (crmCompanyMap == null) {
				crmCompanyMap = businessServiceLocator.getUserProfileRemote().getAllCrmCompanies();
			}
			
			for (String companyId : crmCompanyMap.keySet()) {
				String crmCompanyName = crmCompanyMap.get(companyId);
				if (crmCompanyName.toLowerCase().startsWith(companyNameLike.toLowerCase())) {
					companyList.add(new NameValueDto(companyId, crmCompanyName));
				}
			}
		}
		return companyList;
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = "text/html", headers = "Accept=application/json")
	public @ResponseBody UserProfileDto getUserProfile(HttpSession session, Principal principal) {
		UserProfileDto userProfileDto = new UserProfileDto();
		String username = principal.getName();
		userProfileDto.setUserId(username);
		boolean admin = businessServiceLocator.getLdapSecurityBeanRemote().isMemberOf(username, FrcConstants.LDAP_GROUP_TARION_ADMIN);
		if (admin) {
			userProfileDto.setRoleType(RoleType.TARION_ADMIN);
		}
		return userProfileDto;
	}
	
	@RequestMapping(value = "/searchForCompaniesLike", method = RequestMethod.POST)
	public String searchForCompaniesLike(@RequestParam("companyName") String companyName, Model uiModel) {
		List<UserProfileDto> returnList = businessServiceLocator.getUserProfileRemote().searchForCompaniesLike(companyName);
		uiModel.addAttribute("companyList", returnList);
		uiModel.addAttribute("companyName", companyName);
		return "searchCompanyView";
	}

	@RequestMapping(value = "/searchCompany")
	public String searchForCompaniesLike(Model uiModel)	{
		currentOutageMessage = businessServiceLocator.getContentBeanRemote().getOutageMessage();
		uiModel.addAttribute("currentOutageMessage", currentOutageMessage);
		return "searchCompanyView";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateUser(@RequestParam String userId, @RequestParam String email, @RequestParam String password, @RequestParam(defaultValue="false", required=false) boolean passwordChangeRequired, Principal principal, Model uiModel) {
		try {
			businessServiceLocator.getUserProfileRemote().updateUserEmailPassword(userId, email, passwordChangeRequired, password, principal.getName());
		} catch (Exception e) {
			uiModel.addAttribute("error", "User Update Error Occurred");
		}
		uiModel.addAttribute("action", "update");
		return "updateUserResultView";
	}
	
	@RequestMapping(value = "/updateUser")
	public String updateUser(@RequestParam String userId, Model uiModel) {
		UserProfileDto userProfileDto = businessServiceLocator.getUserProfileRemote().getUserProfileForUserName(userId);
		uiModel.addAttribute("userProfileDto", userProfileDto);
		return "updateUserView";
	}

	
	@RequestMapping(value = "/createAdmin")
	public String createAdmin(Model uiModel)	{
		uiModel.addAttribute("userType", "adminUser");
		return "createUserView";
	}

	@RequestMapping(value = "/createUser")
	public String createUser(Model uiModel)	{
		uiModel.addAttribute("userType", "frcUser");
		return "createUserView";
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createUser(@RequestParam String companyName, @RequestParam String companyId, @RequestParam String userId, @RequestParam String userFirstName, @RequestParam String userLastName, @RequestParam String email, @RequestParam String password, @RequestParam(defaultValue="false", required=false) boolean passwordChangeRequired, Principal principal, Model uiModel) {
//	public @ResponseBody Boolean createUser(@RequestBody UserProfileDto userProfileDto, @RequestParam String password, Principal principal) {
		UserProfileDto userProfileDto = new UserProfileDto();
		userProfileDto.setCompanyName(companyName);
		userProfileDto.setCompanyId(companyId);
		userProfileDto.setUserId(userId);
		userProfileDto.setUserFirstName(userFirstName);
		userProfileDto.setUserLastName(userLastName);
		userProfileDto.setEmail(email);
		if (FrcUtil.isNullOrEmpty(userProfileDto.getCompanyName())) {
			userProfileDto.setRoleType(RoleType.TARION_ADMIN);
			userProfileDto.setCompanyId(FrcConstants.TARION_ADMIN_COMPANY_ID);
			userProfileDto.setCompanyName(FrcConstants.TARION_ADMIN_COMPANY_NAME);
			uiModel.addAttribute("userType", "adminUser");
		} else {
			uiModel.addAttribute("userType", "frcUser");
		}
		
		try {
			businessServiceLocator.getUserProfileRemote().createUser(userProfileDto, password, principal.getName());
		} catch (Exception e) {
			uiModel.addAttribute("error", "User Creation Error Occurred");
		}
		// return "redirect:/user/createUserResult";
		crmCompanyMap = null;
		return "createUserResultView";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteUser(@RequestParam String userId, Principal principal, Model uiModel) {
		try {
			businessServiceLocator.getUserProfileRemote().deleteUser(userId, principal.getName());
		} catch (Exception e) {
			uiModel.addAttribute("error", "User Deletion Error Occurred");
		}
		
		uiModel.addAttribute("action", "delete");
		uiModel.addAttribute("userId", userId);
		return "updateUserResultView";
	}

	
}