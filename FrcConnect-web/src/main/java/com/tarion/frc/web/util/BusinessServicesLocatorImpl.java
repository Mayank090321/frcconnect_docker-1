/* 
 * 
 * BusinessServicesLocator.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.util;


import org.springframework.stereotype.Service;

import com.tarion.frc.content.ContentBeanRemote;
import com.tarion.frc.project.ProjectBeanRemote;
import com.tarion.frc.userprofile.LdapSecurityBeanRemote;
import com.tarion.frc.userprofile.UserProfileBeanRemote;

/**
 * The Interface BusinessServicesLocator.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
@Service
public class BusinessServicesLocatorImpl extends JNDIServicesLocator implements BusinessServicesLocator {

    public UserProfileBeanRemote getUserProfileRemote() {
		return (UserProfileBeanRemote) getBean(UserProfileBeanRemote.class);
    }

	@Override
	public LdapSecurityBeanRemote getLdapSecurityBeanRemote() {
		return (LdapSecurityBeanRemote) getBean(LdapSecurityBeanRemote.class);
	}
	
	@Override
	public ProjectBeanRemote getProjectBeanRemote() {
		return (ProjectBeanRemote) getBean(ProjectBeanRemote.class);
	}
	
	@Override
	public ContentBeanRemote getContentBeanRemote() {
		return (ContentBeanRemote) getBean(ContentBeanRemote.class);
	}
}
