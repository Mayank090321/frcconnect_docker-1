/* 
 * 
 * LdapAuthenticationProvider.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.security;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.userprofile.dto.UserProfileDto.RoleType;
import com.tarion.frc.web.util.BusinessServicesLocator;

/**
 * Ldap Authentication Provider
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-25
 * @version 1.0
 */
@Service("ldapAuthenticationProvider")
public class LdapAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	@Autowired
	private BusinessServicesLocator businessServiceLocator;
	
	public LdapAuthenticationProvider() {

	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails arg0, UsernamePasswordAuthenticationToken arg1) throws AuthenticationException {

	}

	@Override
	protected UserDetails retrieveUser(String userName, UsernamePasswordAuthenticationToken auth) throws AuthenticationException {
		
		UserDetails userDetails = null;
		String password = (String) auth.getCredentials();

		boolean authenticated = false;
		try {
			authenticated = businessServiceLocator.getLdapSecurityBeanRemote().isAllowed(userName, password);
			
		} catch (NamingException e) {
			authenticated = false;
		} 

		List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		if (authenticated) {
			boolean admin = businessServiceLocator.getLdapSecurityBeanRemote().isMemberOf(userName, FrcConstants.LDAP_GROUP_TARION_ADMIN);
			if (admin) {
				SimpleGrantedAuthority simpleAuthority = new SimpleGrantedAuthority(RoleType.TARION_ADMIN.toString());
				grantedAuthorities.add(simpleAuthority);
			} else {
				SimpleGrantedAuthority simpleAuthority = new SimpleGrantedAuthority(RoleType.FRC_USER.toString());
				grantedAuthorities.add(simpleAuthority);
			}
			userDetails = new User(userName, password, grantedAuthorities);
		} else {
			throw new BadCredentialsException("Invalid UserID or Password");
		}
		
		return userDetails;
	}

}
