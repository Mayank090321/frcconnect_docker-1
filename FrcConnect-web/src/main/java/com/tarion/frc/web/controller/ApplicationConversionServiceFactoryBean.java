/* 
 * 
 * ApplicationConversionServiceFactoryBean.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.controller;

import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

/**
 * A central place to register application converters and formatters. 
 */
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
	}
}
