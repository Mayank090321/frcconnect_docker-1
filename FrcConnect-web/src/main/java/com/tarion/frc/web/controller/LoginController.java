/* 
 * 
 * LoginController.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.web.controller;

import java.security.Principal;

import javax.naming.AuthenticationException;
import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.userprofile.dto.UserProfileDto;
import com.tarion.frc.util.LoggerUtil;
import com.tarion.frc.web.util.BusinessServicesLocator;

/**
 * Login Controller.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-25
 * @version 1.0
 */
@RequestMapping("/login")
@Controller
public class LoginController {

	@Autowired
	private BusinessServicesLocator businessServiceLocator;


	@RequestMapping(produces = "text/html")
	public String redirectToCorrespondingHomePageBasedOnUserRole(Principal principal, Model uiModel) {
		if (principal == null) {
			uiModel.addAttribute("authError", true);
			return "indexView";
		} else {
			String username = principal.getName();
			boolean passwordChangeRequired = businessServiceLocator.getLdapSecurityBeanRemote().isPasswordChangeRequired(username);
			String redirect = "redirect:/login/passwordChange";
			if (!passwordChangeRequired) {
				redirect = redirectHomePage(username);
			}
			return redirect;
		}
	}
	
	@RequestMapping(value = "/forgotPasswordSend", method = RequestMethod.POST)
	public String forgotPasswordSend(@RequestParam String username, @RequestParam String email, Model uiModel) {
		// verify username and email
		UserProfileDto userProfileDto = businessServiceLocator.getUserProfileRemote().getUserProfileForUserName(username);
		boolean errorOccured = false;
		if (userProfileDto != null) {
			String emailFromDto = userProfileDto.getEmail();
			if (emailFromDto.equalsIgnoreCase(email)) {
				// generate temp password
				String generatedPassword = businessServiceLocator.getLdapSecurityBeanRemote().getRandomPassword();
				// update password with generated and must change pwd and send email
				try {
					businessServiceLocator.getUserProfileRemote().updateUserEmailPassword(username, email, true, generatedPassword, FrcConstants.TARION_ADMIN_COMPANY_NAME);
				} catch (Exception e) {
					LoggerUtil.logError(LoginController.class, "UserId " + username + " does not exist in the system");
					errorOccured = true;
				}
			} else {
				errorOccured = true;
			}
		} else {
			errorOccured = true;
		}
		uiModel.addAttribute("forgotPasswordSentSuccess", !errorOccured);
		return "forgotPasswordResultView";
	}

	@RequestMapping(value = "/passwordChange", method = RequestMethod.GET)
	public String changePassword() {
		return "changePasswordView";
	}
	@RequestMapping(value = "/passwordUpdate", method = RequestMethod.POST)
	public String passwordUpdate(@RequestParam String oldPassword, @RequestParam String newPassword, Principal principal, Model uiModel) {
		String redirect = null;
		boolean oldPasswordCorrect = false;
		try {
			businessServiceLocator.getLdapSecurityBeanRemote().isAllowed(principal.getName(), oldPassword);
			oldPasswordCorrect = true;
		} catch (AuthenticationException e) {
			oldPasswordCorrect = false;
		} catch (NamingException e) {
			oldPasswordCorrect = false;
		}
		if (oldPasswordCorrect) {
			businessServiceLocator.getLdapSecurityBeanRemote().changePassword(principal.getName(), newPassword, false);
			uiModel.addAttribute("passwordUpdateSuccess", true);
		} else {
			uiModel.addAttribute("passwordUpdateSuccess", false);
		}
		return "changePasswordResultView";
	}
	
	@RequestMapping(value = "/passwordUpdateSuccess", method = RequestMethod.GET)
	public String passwordUpdateSuccess(Principal principal) {
		return redirectHomePage(principal.getName());
	}
	
	private String redirectHomePage(String username) {
		boolean admin = businessServiceLocator.getLdapSecurityBeanRemote().isMemberOf(username, FrcConstants.LDAP_GROUP_TARION_ADMIN);
		if (admin) {
			return "redirect:/user/searchCompany";
		} else {
			return "redirect:/project/frcUserLogin";
		}
	}
}