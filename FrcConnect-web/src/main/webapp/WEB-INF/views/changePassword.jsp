<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="standard-div">
	<div class="eight-hundred-width">
		<div class="fifty-margin" id="create-user">
			<h2 class="text-align">Change Password</h2>
			<br>
			<spring:url value="passwordUpdate" var="passwordUpdate_url"></spring:url>
			<form method="post" name="passwordUpdate" id="passwordUpdate" action="${passwordUpdate_url}">
				<div class="row">
					<label class="form-label">Old Password:</label>
					<input class="form-control userInputClass" data-errorSpan="oldPasswordErrorMessage" type="password" name="oldPassword" id="oldPassword" /> 
					<span class="oldPasswordErrorMessage form-label">Please Insert Old Password</span>
				</div>
				<div class="row">
					<label class="form-label">New Password:</label>
					<input class="form-control userInputClass" data-errorSpan="newPasswordErrorMessage" type="password" name="newPassword" id="newPassword" /> 
					<span class="newPasswordErrorMessage form-label">Please Insert New Password</span>
				</div>
				<div class="row">
					<label class="form-label">Confirm Password:</label>
					<input class="form-control userInputClass" data-errorSpan="newPasswordConfirmErrorMessage" type="password" name="newPasswordConfirm" id="newPasswordConfirm" />
					<span class="newPasswordConfirmErrorMessage form-label">Passwords do not match</span>
				</div>

				<div class="row-buttons">
					<a href="#" class="btn btn-primary float-right">Save Changes</a>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
  $(document).ready(function() {
	$(".userInputClass").keypress(function() {
		$("."+$(this).attr("data-errorSpan")).hide();
	});
	$('#passwordUpdate a.btn-primary').on("click", function() {
		var errorOccured = 0;
		if ($("#oldPassword").val() == "") {
			$('.oldPasswordErrorMessage').show();
			errorOccured = 1;
		}  else {
			$('.oldPasswordErrorMessage').hide();
		}
		if ($("#newPassword").val() == "") {
			$('.newPasswordErrorMessage').show();
			errorOccured = 1;
		}  else {
			$('.newPasswordErrorMessage').hide();
		}
		if ($("#newPasswordConfirm").val() != $("#newPassword").val() ) {
			$('.newPasswordConfirmErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.newPasswordConfirmErrorMessage').hide();
		}

		if (errorOccured == 0) {
			$('#passwordUpdate').submit();
		}
	});
  });
</script>