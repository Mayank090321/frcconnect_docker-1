<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="standard-div">
	<div id="loginFrame">
		<c:choose>
			<c:when test="${action == 'update'}">
				<c:choose>
					<c:when test="${error == null}">
						<h2>User Edited</h2>
						<br>
						<p>The changes to the user have been updated. If you have changed the users password, and new email has been sent to the user.</p>
					</c:when>
					<c:otherwise>
						<h2>User Update Error</h2>
						<br>
						<p>${error}</p>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${error == null}">
						<h2>User Deleted.</h2>
						<br>
						<p>The user ${userId} has been deleted.</p>
					</c:when>
					<c:otherwise>
						<h2>User Deletion Error</h2>
						<br>
						<p>${error}</p>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>	

		<spring:url value="searchCompany" var="searchCompany_url"></spring:url>
		<div class="row">
			<a href="${searchCompany_url}" class="btn btn-primary float-right">OK</a>
		</div>
	</div>
</div>