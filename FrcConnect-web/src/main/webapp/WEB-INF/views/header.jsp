<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div class="nav-header-top">
	<div class="row">
			<div class="nav-header-left">
				<a href="http://www.tarion.com" class="brandLogo" title="Return Home" target="_blank">
				</a>	
			</div>
			
			<security:authorize access="isAuthenticated()">
			<div class="logoutLink">
				<spring:url value="/resources/j_spring_security_logout" var="logout_url"></spring:url>
				<a href="${logout_url}">Logout</a>
			</div>
		</security:authorize>
	</div>
</div>	
					
<div class="nav-header">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="collapse navbar-collapse">
      			<ul class="nav navbar-nav">
      				<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="text-align: center;">FRC Application Form (BQS)<br><p class="smallp"></p><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%201%20-%20FRC%20Bulletin%2019R%20Qualification%20Status-2017.pdf">Module 1 - FRC Bulletin 19R Qualification Status</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%201A%20-%20FRC%20Application%20for%20Bulletin%2019R%20Qualification%20Status-2017.pdf">Module 1A - FRC Application for Bulletin 19R Qualification Status</a>
							</li>
						</ul>
					</li>
      				<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="text-align: center;">BB19R Reports<br><p class="smallp">(Construction Start Date on or before December 31, 2016)</p><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2017-09/BB19R-Condominium-Projects-Design-and-Field-Review-Reporting.pdf">Builder Bulletin 19R</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Scope%20of%20Work%20Proposal%20-%20High%20Rise%20Townhouse%20Combined_2.pdf">Scope of Work Proposal - High Rise & Townhouse Combined</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Scope%20of%20Work%20Proposal%20-%20High-Rise%20Projects_3.pdf">Scope of Work Proposal - High-Rise Projects</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Scope%20of%20Work%20Proposal%20-%20Townhouse%20Projects_4.pdf">Scope of Work Proposal - Townhouse Projects</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Milestone%20Report%20-%20Deficiency%20Resolution%20Tracking%20Sheet_5.pdf">Milestone Report - Deficiency Resolution Tracking Sheet</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Milestone%20Report%20-%20Tracking%20Summary_6.pdf">Milestone Report - Tracking Summary</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Milestone%20Report%20-%20Tracking%20Summary_7.pdf">Milestone Report - Tracking Summary</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Scope%20of%20Work%20-%20Description%20of%20Notional%20Buildings_8.pdf">Scope of Work - Description of Notional Buildings</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Scope%20of%20Work%20-%20Level%20of%20Effort%20Guideline%20Tables_9.pdf">Scope of Work - Level of Effort Guideline Tables</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Design%20Certificate_10.pdf">Design Certificate</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_60%20Day%20Report_11.pdf">60 Day Report</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Field%20Review%20Declaration_12.pdf">Field Review Declaration</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R/BB19R_Notice%20of%20Completion_13.pdf">Notice of Completion</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="text-align: center;">BB19R Reports<br><p class="smallp">(Construction Start Date on or after January 1, 2017)</p><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2020-08/Builder-Bulletin-19R-2020-08-28_0.pdf">Builder Bulletin 19R</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%202A%20-%20Scope%20of%20Work%20Proposal-2017.pdf">Module 2A - Scope of Work Proposal</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/sites/default/files/2020-09/BB19R_Module2A_ScopeOfWork_09_2020.pdf">Module 2A - Scope of Work Proposal (updated September 1, 2020)</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%203%20-%20Scope%20of%20Work%20-%20Notional%20Building%20Guidelines%20for%20Level%20of%20Effort-2017.pdf">Module 3 - Scope of Work - Notional Building Guidelines for Level of Effort</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%203A%20-%20Scope%20of%20Work%20-%20Notional%20Building%20Guidelines%20for%20Level%20of%20Effort-2017.pdf">Module 3A - Scope of Work - Notional Building Guidelines for Level of Effort</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204%20-%20Appendix%20-%20Design%20Review-2017.pdf">Module 4 - Appendix - Design Review</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204%20-%20Appendix%20-%20What%20a%20Deficiency%20is%20and%20When%20to%20Report%20One-2017.pdf">Module 4 - Appendix - What a Deficiency Is and When to Report One</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204%20-%20Field%20Review%20Consultant%20Reporting%20Requirements-2017.pdf">Module 4 - Field Review Consultant Reporting Requirements</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204A%20-%20Appendix%20-%20Design%20Certificate-2017.pdf">Module 4A - Appendix - Design Certificate</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204B%20-%2060-Day%20Report-2017.pdf">Module 4B - 60 Day Report</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204C%20-%20Milestone%20Report%20-%20Deficiency%20Summary-2017.pdf">Module 4C - Milestone Report - Deficiency Summary</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204D%20-%20Field%20Review%20Declaration-2017.pdf">Module 4D - Field Review Declaration</a>
							</li>
							<li>
								<a target="_blank" href="http://www.tarion.com/resources/publications/Documents/BB19R2017/Module%204D%20-%20The%20Final%20Report-2017.pdf">Module 4D - The Final Report</a>
							</li>
							

						</ul>
					</li>
					<li class="dropdown">
          				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="text-align: center;">BB19R Reports<br>BB51 Condo Conversions<br><p class="smallp">(For use on approved RCCP's after January 1, 2018)</p><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li>
								<a target="_blank" href="https://tarion.com/sites/default/files/2018-01/Builder-Bulletin-19R-D5-Dec-19-2017_0.pdf">Builder Bulletin 19R</a>
							</li>
							<li>
								<a target="_blank" href="https://tarion.com/sites/default/files/2018-02/BB51-Residential%20Condominium%20Conversion%20Projects%20-%20February%208%202018.pdf">Builder Bulletin 51</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/BB51%20Requirements%20for%20FRCs_0.pdf">BB51 Requirements for FRCs</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%20Scope%20of%20Work%20Proposal-2018-04-17_0.pdf">RCCP - Scope of Work Proposal</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%20Design%20Certificate%20-%20General-2018-04-16_0.pdf">RCCP - Design Certificate - General</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%20Design%20Certificate%20-%20Code%20Consultant-2018-04-23_0.pdf">RCCP - Design Certificate - Code Consultant</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%2060%20Day%20Report-2018-04-16_0.pdf">RCCP - 60 Day Report</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%20Milestone%201%202%203%20and%204-2018-05-15.pdf">RCCP - Milestone Report 1, 2, 3 and 4</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%20Milestone%205-2018-04-20_0.pdf">RCCP - Milestone Report 5</a>
							</li>
							<li>
								<a target="_blank" href="https://www.tarion.com/sites/default/files/2018-05/RCCP%20-%20Field%20Review%20Declaration-2018-04-16_0.pdf">RCCP - Field Review Declaration</a>
							</li>
							
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</div>