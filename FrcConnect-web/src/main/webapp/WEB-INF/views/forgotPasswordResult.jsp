<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:url value="/" var="forgotPasswordSentSuccess_url"></spring:url>
<spring:url value="/forgotPassword" var="forgotPasswordSentFailure_url"></spring:url>

<div class="standard-div">
	<div id="loginFrame">
		<c:choose>
			<c:when test="${forgotPasswordSentSuccess}">
				<div class="row">
					<span class="green-text">Temporary Password has been sent to your email! </span>
				</div>
				<br>
				<div class="row">
					<a href="${forgotPasswordSentSuccess_url}" class="btn btn-primary float-right">Continue</a>
				 </div>
			</c:when>
			<c:otherwise>
				<div class="row">
					<span class="red-text">Either your username does not exist or email is incorrect. Please try again.
					</span>
				</div>
				<br>
				<div class="row">
					<a href="${forgotPasswordSentFailure_url}" class="btn btn-primary float-right">Continue</a>
				 </div>
			</c:otherwise>
		</c:choose>
	</div>
</div>

