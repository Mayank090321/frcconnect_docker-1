<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="standard-div ">
	<div id="loginFrame">
		<h2>Forgot Password</h2>
		<br>
		<spring:url value="login/forgotPasswordSend" var="forgotPasswordSend_url"></spring:url>
		<form name="f" action="${forgotPasswordSend_url}" method="POST">
			<div class="row">
				<div class="input-group">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-user" />
					</div>
					<input class="form-control username" type="text" placeholder="User ID" name="username" />
				</div>
			</div>
			<div class="row">
				<div class="input-group">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-user" />
					</div>
					<input class="form-control em" type="text" placeholder="User Email" name="email" />
				</div>
			</div>
			<div class="row">
				<br>
				<div class="" id="forgotPasswordMessage">
				</div>
			</div>
			<div class="row">
					<button id="loginButton" type="submit" class="btn btn-primary float-right">Send Temporary Password</button>
			</div>
		</form>
	</div>
</div>