<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="standard-div">
	<div id="loginFrame">
		<c:choose>
			<c:when test="${userType == 'frcUser'}">
				<c:choose>
					<c:when test="${error == null}">
						<h2>Create New FRC Connect User</h2>
						<br>
						<p>The new user has been created. An email with a User ID & Password has been sent to the new user.</p>
					</c:when>
					<c:otherwise>
						<h2>Create New FRC Connect User Error</h2>
						<br>
						<p>${error}</p>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when test="${error == null}">
						<h2>New Administrator Created.</h2>
						<br>
						<p>The new administrator has been created. An email with a User ID & Password has been sent to the new administrator.</p>
					</c:when>
					<c:otherwise>
						<h2>Create New Administrator Error</h2>
						<br>
						<p>${error}</p>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>	
		
		<spring:url value="searchCompany" var="searchCompany_url"></spring:url>	
		<div class="row">
			<a href="${searchCompany_url}" class="btn btn-primary float-right">OK</a>
		</div>
	</div>
</div>



