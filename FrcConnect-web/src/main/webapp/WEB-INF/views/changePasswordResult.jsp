<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:url value="passwordUpdateSuccess" var="passwordUpdateSuccess_url"></spring:url>
<spring:url value="passwordChange" var="passwordChange_url"></spring:url>

<div class="standard-div">
	<div id="loginFrame">
		<c:choose>
			<c:when test="${passwordUpdateSuccess}">
				<div class="row">
					<p class="green-text">Password successfully changed!</p>
				</div>
				<br />
				<div class="row">
					<a href="${passwordUpdateSuccess_url}" class="btn btn-primary float-right">Continue</a>
				 </div>
			</c:when>
			<c:otherwise>
				<div class="row">
					<p class="red-text">Old Password Incorrect!</p>
				</div>
				<br />
				<div class="row">
					<a href="${passwordChange_url}" class="col-sm-2 btn btn-primary">Continue</a>
				 </div>
			</c:otherwise>
		</c:choose>
	</div>
</div>

