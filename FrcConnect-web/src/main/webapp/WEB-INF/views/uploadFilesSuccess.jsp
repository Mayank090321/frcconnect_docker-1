<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div class="standard-div">
	<div id="loginFrame">
		<c:choose>
			<c:when test="${uploadStatus == null}">
				<h2>Documents Uploaded Successfully</h2>
				<br>
				<p>${enrolmentNumber} ${currentDateTime} Your document(s) have been successfully uploaded. Your information will be reviewed shortly.</p> 
				<spring:url value="uploadFilesSuccess" var="uploadFilesSuccess_url"></spring:url>
				<div class="row">
					<a href="${uploadFilesSuccess_url}" class="btn btn-primary float-right">OK</a>
				</div>
			</c:when>
			<c:otherwise>
				<h2>Documents Failed to Upload</h2>
				<p>${enrolmentNumber} ${currentDateTime} Your document(s) failed to upload. Please try again.<br /> 
				If this problem persists, please contact us at <a href="mailto:b19@tarion.com" target="_top">b19@tarion.com</a></p>
				<spring:url value="uploadPage" var="uploadPage_url"></spring:url>
				<form method="post" name="form" id="form" action="${uploadPage_url}">
					<input class="form-control" type="hidden" name="vbNumber" value="${vbNumber}" />
					<input  class="form-control" type="hidden" name="enrolmentNumber" value="${enrolmentNumber}" />
					<input type="submit" class="btn btn-primary float-right" value="Return to upload page" />
				</form>
			</c:otherwise>
		</c:choose>	
	</div>
</div>

