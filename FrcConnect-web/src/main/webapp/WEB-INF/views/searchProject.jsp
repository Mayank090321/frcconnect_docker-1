<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script type="text/javascript">
	function handleSelectChange() {
		searchProjectAddress.hidden = true;
		searchProjectName.hidden = true;
		searchProjectEnrolment.hidden = true;
		if (document.getElementById('searchSelect').selectedIndex == 1) {
			searchProjectAddress.hidden = false;
		}
		if (document.getElementById('searchSelect').selectedIndex == 2) {
			searchProjectName.hidden = false;
		}
		if (document.getElementById('searchSelect').selectedIndex == 3) {
			searchProjectEnrolment.hidden = false;
		}
	}
	
</script>

<spring:url value="searchForProjectsLike" var="searchForProjectsLike_url"></spring:url>
<div class="standard-div">
	<div class="standard-width">
		<div class="fifty-margin">
			<form method="post" name="searchForProjectsLike" action="${searchForProjectsLike_url}">
				<div class="row grey-background shadow">
					<div class="search-project">
						<label for="searchSelect">Search for Projects:</label>
						<br>
						<div class="input-group">
							<select class="form-control" id="searchSelect" name="searchSelect" onChange="handleSelectChange()">
								<option selected value="Search By (select one)">Search By (select one)</option>
								<option value="Address">Address</option>
								<option value="Project Name">Project Name</option>
								<option value="Enrolment Number">Enrolment Number</option>
							</select>
							<div id="searchProjectAddress" hidden="true">
								<div class="row">
									<input class="form-control" type="text" name="streetNumber" id="streetNumber" placeholder="Street No." />
									<input class="form-control" type="text" name="streetName" id="streetName" placeholder="Street Name" />
								</div>
							</div>
							<div id="searchProjectName" hidden="true">
								<input class="form-control" type="text" name="projectName" id="projectName" placeholder="Project Name"/>
							</div>
							<div id="searchProjectEnrolment" hidden="true">
								<input class="form-control" type="text" name="projectEnrolment" id="projectEnrolment" placeholder="Project Enrolment #"/>
							</div>
						</div>
					</div>
					<div class="search-button">
						<div class="search-companies-buttons">
							<button class="btn btn-primary float-right" aria-label="Search">Search</button>
						</div>
					</div>
				</div>
			</form>

			<c:if test="${outageMessage != null}">
			<div class="outageMessage twenty-margin">
			<p>${outageMessage}</p>
			</div>
			</c:if>


			<div id="projectSearch">
				<br>
				<p><c:if test="${projectList != null && fn:length(projectList) > 0}"></p>
					<h3>Your Active Projects</h3>
					<table class="table searchProjectTable">
						<thead>
							<tr>
								<th>Enrolment No.</th>
								<th>Project Name</th>
								<th>Civic Address</th>
								<th>V/B Name</th>
								<th>Phase No.</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${projectList}" var="dto">
								<tr>
									<spring:url value="uploadPage?enrolmentNumber=${dto.enrolmentNumber}&vbNumber=${dto.vbNumber}" var="uploadPage_url"></spring:url>
									<td><a href="${uploadPage_url}">${dto.enrolmentNumber}</a></td>
									<td>${dto.projectName}</td>
									<td>${dto.civicAddress}</td>
									<td>${dto.vbName}</td>
									<td>${dto.phaseNumber}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
				<c:if test="${projectList == null || fn:length(projectList) == 0}">
				No Project found.
				</c:if>
			</div>
		</div>	
	</div>
</div>