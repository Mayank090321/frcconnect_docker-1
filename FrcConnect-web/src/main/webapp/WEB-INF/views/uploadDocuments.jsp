<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>



<div class="standard-div">
	<div class="standard-width">
		<div class="fifty-top-margin">
			<h3>Upload Documents for <b>${enrolmentNumber}</b></h3>
			<p><b>Civic Address:</b> ${streetAddress} ${suiteNum}, ${city} ${province}, ${postalCode}</p>
			<p><b>Assigned Warranty Services Representative:</b> ${wsrName}</p>
			<p><b>Email:</b> <a href="emailto: '+${wsrEmail}+'"> ${wsrEmail} </a></p>
			<p>Please select the documents you would like to upload. You may upload more than one file at a time. If the document you would like to submit is not listed, please contact us at <a href="mailto:b19@tarion.com">b19@tarion.com</a></p>

			<div class="twenty-margin">
			<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary open-file-upload-modal">
				Add a Document
				</button>
			</div>

			<div class="row">
				<table class="table">
					<thead>
						<tr>
							<th>Document Name</th>
							<th>Document Type</th>
							<th><!-- Delete Icon --></th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${documentUpoadList != null && fn:length(documentUpoadList) > 0}">
						<c:forEach items="${documentUpoadList}" var="dto">
							<tr>
								<td>${dto.filename}</td>
								<td>${dto.templateType.value}</td>
								<td>
									<spring:url value="removeFile?filename=${dto.filename}" var="removeFile_url"></spring:url>
									<a href="${removeFile_url}" class="btn btn-default float-right"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
				<c:if test="${documentUpoadList == null || fn:length(documentUpoadList) == 0}">
				no documents have been added yet.
				</c:if>
			</div>
			<div class="twenty-margin">
				<c:if test="${documentUpoadList != null && fn:length(documentUpoadList) > 0}">
					<button type="button" class="btn btn-primary upload-all-files float-right">Upload</button>
					<spring:url value="uploadAllFiles" var="uploadAllFiles_url"></spring:url>
					<form id="uploadAllFilesForm" action="${uploadAllFiles_url}" method="post">
					</form>
				</c:if>
				<c:if test="${documentUpoadList == null || fn:length(documentUpoadList) == 0}">
					<button type="button" class="btn btn-default float-right" disabled="disabled">Upload</button>
				</c:if>
				<button type="button" class="btn btn-default open-cancel-modal">Cancel</button>
			</div>
			<div class="row"><h4>Previously Submitted Documents</h4></div>
			<div class="row">
				<c:if test="${previousDocumentsList == null || fn:length(previousDocumentsList) == 0}">
					No documents have been submitted yet.
				</c:if>
				<c:if test="${previousDocumentsList != null && fn:length(previousDocumentsList) > 0}">
					<table id="previousDocumentsTable" class="table table-bordered dataTable">
						<thead>
							<tr>
								<th>Description</th>
								<th>Received Date</th> 
								<th>Decision</th> 
								<th>Decision Date</th>
								<th>On Time</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${previousDocumentsList}" var="dto">
								<tr>
									<td>${dto.description}</td>
									<td data-order="${dto.receivedDate.time}"><fmt:formatDate value="${dto.receivedDate}" pattern="MM/dd/yyyy"/></td>
									<td>${dto.decision}</td>
									<td data-order="${dto.decisionDate.time}"><fmt:formatDate value="${dto.decisionDate}" pattern="MM/dd/yyyy"/></td>
									<td>${dto.onTime}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="fileUploadModal" tabindex="-1" role="dialog" aria-labelledby="fileUploadModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="fileUploadModalLabel">Upload Document  (max file size 25MB)</h4>
			</div>
			<div class="modal-body">
			  <spring:url value="uploadFile" var="uploadFile_url"></spring:url>
			  <form id="fileUploadForm" action="${uploadFile_url}" method="post" enctype="multipart/form-data">
				<fieldset>
					<input type="file" name="uploadedFile" id="uploadedFile" />
					<span class="fileErrorMessage">Please Select File to Upload</span>
					<span class="fileSizeErrorMessage">File size exceeds 25MB</span>
					<select class="form-control twenty-margin" id="templateTypeSelect" name="templateTypeSelect">
						<option selected value="-1">Select Document Type</option>
						<c:if test="${allTemplateTypeList != null && fn:length(allTemplateTypeList) > 0}">
							<c:forEach items="${allTemplateTypeList}" var="dto">
								<option value="${dto.name }">${dto.value }</option>
							</c:forEach>
						</c:if>
					</select>
					<span class="documentTypeErrorMessage">Please Select Document Type</span>
					<div id="sixtyDayType">
						<div class="col-fifty">
							<label class="form-label" for="startDate">From Date: </label>
							<input class="form-control" type="text" name="startDate" id="startDate" />
						</div>
						<div class="col-fifty">
							<label class="form-label" for="endDate">To Date: </label>
							<input class="form-control" type="text" name="endDate" id="endDate" />
						</div>
						<div class="col-fifty">
							<label class="form-label" for="reportNumberUpdate">Report No.</label>
							<input class="form-control" type="text" name="reportNumberUpdate" id="reportNumberUpdate" value="${reportNumber}"/>
						</div>

						<p class="dateErrorMessage">To Date must be after From Date</p>

					</div>
					<!-- Allow form submission with keyboard without duplicating the dialog button -->
				</fieldset>
			  </form>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary float-right">Add Document</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="cancelModalLabel">Cancel Upload</h4>
      </div>
      <div class="modal-body">
		  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Are you sure you wish to cancel document upload?</p>   
  		  <spring:url value="cancelUpload" var="cancel_url"></spring:url>
		  <form method="post" id="cancelForm" name="cancel" action="${cancel_url}">
		  <!-- 
				<input type="hidden" name="userId" value="${userProfileDto.userId}" /> 
				-->
		  </form>
  	  </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-danger">Cancel Upload</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Continue Upload</button>
      </div>
    </div>
  </div>
</div>

<script>
  var sixtyDayCode = ${sixtyDayReportCode};
  $(document).ready(
	function() {
		
	$('#previousDocumentsTable').DataTable({
		"pageLength": 10,
		"lengthChange": false,
		"searching": false,
    	"info": false,
    	"order": [],
    	"ordering": true,
    	"fnDrawCallback": function (oSettings) {
			var pgr = $(oSettings.nTableWrapper).find('.dataTables_paginate')
			if (oSettings._iDisplayLength >= oSettings.fnRecordsDisplay()) {
				pgr.hide();
			} else {
				pgr.show()
			}
	    }
	});
	$('button.open-cancel-modal').on("click", function() {
		$("#cancelModal").modal({backdrop: "static", keyboard: false});
	});
	$('#cancelModal').on("show.bs.modal", function(e) {
		$('#cancelModal button.btn-danger').on("click", function() {
			$('#cancelForm').submit();
		})
	});
	$("button.upload-all-files" ).on("click", function() {
		$('#uploadAllFilesForm').submit();
	});
	$("#startDate" ).datepicker();
	$("#endDate" ).datepicker();
	$("#templateTypeSelect").change(function() {
		if ($(this).val() == sixtyDayCode) {
			$("#sixtyDayType").show();
		} else {
			$("#sixtyDayType").hide();
		}
	});
	$('button.open-file-upload-modal').on("click", function() {
		$("#fileUploadModal").modal({backdrop: "static", keyboard: false});
	});
	$('#fileUploadModal').on("show.bs.modal", function(e) {
		$('#fileUploadModal button.btn-primary').on("click", function() {
			var uploadedFile = $('#uploadedFile');
			if (uploadedFile.val() == "") {
				$('.fileErrorMessage').show();
				$('.fileSizeErrorMessage').hide();
			} if (uploadedFile[0].files[0].size > 25*1024*1024) {
				$('.fileSizeErrorMessage').show();
				$('.fileErrorMessage').hide();
			} else {
				if ($("#templateTypeSelect").val() == -1) {
					$('.documentTypeErrorMessage').show();
				} else {
					if ($("#templateTypeSelect").val() == sixtyDayCode) {
						var start= new Date($('#startDate').val());
						var end= new Date($('#endDate').val());
						if (start < end) {
							$('#fileUploadForm').submit();
						} else {
							$(this).addClass("btn-danger").removeClass("btn-primary");
							$('.dateErrorMessage').show();
						}
					} else {
						$('#fileUploadForm').submit();
					}	
				}
			}
		})
	});
  });
  
  // Sort Alphabetically
		sortSelectOptions('#templateTypeSelect', true);
		
  // Sorts Select Options, can skip the first select option
	function sortSelectOptions(selector, skip_first) {
    var options = (skip_first) ? $(selector + ' option:not(:first)') : $(selector + ' option');
    var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value, s: $(o).prop('selected') }; }).get();
    arr.sort(function(o1, o2) {
      var t1 = o1.t.toLowerCase(), t2 = o2.t.toLowerCase();
      return t1 > t2 ? 1 : t1 < t2 ? -1 : 0;
    }); 
    options.each(function(i, o) {
        o.value = arr[i].v;
        $(o).text(arr[i].t);
        if (arr[i].s) {
            $(o).attr('selected', 'selected').prop('selected', true);
        } else {
            $(o).removeAttr('selected');
            $(o).prop('selected', false);
        }
    }); 
}
</script>

