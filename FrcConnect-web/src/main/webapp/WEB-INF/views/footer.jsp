<div class="footer-btm">
	<div class="inner-footer">
		<div class="footer-left">
			<a href="http://www.tarion.com/Pages/Site-Map.aspx">Site Map</a>
			<a href="http://www.tarion.com/Pages/Privacy.aspx">Privacy</a>
			<a href="http://www.tarion.com/Pages/Terms-of-Use.aspx">Terms Of Use</a>
			<a href="http://www.tarion.com/contact/Pages/default.aspx">Contact</a>
			<a href="http://www.tarion.com/about/accessibility/Pages/default.aspx">Accessibility</a>
		</div>
		<div class="footer-right">
			<span class="copy">Copyright 2016 Tarion</span>
		</div>
	</div>
</div>


