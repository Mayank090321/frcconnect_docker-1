<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<spring:url value="searchForCompaniesLike" var="searchForCompaniesLike_url"></spring:url>
<div class="standard-div">
	<div class="standard-width">
		<div class="fifty-top-margin">
			<form method="post" name="searchForCompaniesLike" action="${searchForCompaniesLike_url}">
				<div class="row grey-background shadow">
					<div class="col-fifty">
						<label for="searchString">Search for Users by FRC Company Name:</label>
						<br>
						<div class="input-group">
							<input class="form-control" type="text" aria-label="Text input with multiple buttons" name="companyName" value="${companyName}" />
							<div class="input-group-btn">
								<button class="button-no-underline" aria-label="Search">
									&nbsp; <span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;
								</button>
							</div>
						</div>
					</div>
					<div class="col-fifty">
						<div class="search-companies-buttons">
							<a href="createUser" class="btn btn-primary float-right">Create New User</a>
							<a href="createAdmin" class="btn btn-default float-right" id="createAdminBtn">Create New Admin</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	
		<div class="fifty-margin width-fifty">
			<div id="adminSearchText">
			  <c:if test="${companyList == null }">
				<h3>Search Existing Users -or- Create New Users</h3>
				<p>Create new or modify existing users.</p>
				<p>Begin by filling out the search field above, or by selecting the "Create New User" button above. You may also create a new FRC Connect Administrator as well.</p>
				<p>If there are any messages regarding system outages that you would like the user to see on the home screen, please fill in the box below:</p>
				<spring:url value="saveOutageMessage" var="saveOutageMessage_url"></spring:url>
				<div class="col-fifty">
					<form method="post" name="outageMessage" action="${saveOutageMessage_url}">
						<br />
						<textarea rows="6" placeholder="Enter message here." name="outageMessage">${currentOutageMessage}</textarea><br /><br />
						<button class="btn btn-primary float-right" aria-label="Save">Save</button>
					</form>
				</div>
			  </c:if>
			</div>
	

			<div id="adminSearch">
				<c:if test="${companyList != null}">
					<h3>User Search Results</h3>
				</c:if>
				<c:if test="${companyList != null && fn:length(companyList) == 0}">
					No users belonging to searched company found.
				</c:if>
				<c:if test="${companyList != null && fn:length(companyList) > 0}">
					<table class="table">
						<thead>
							<tr>
								<th>User ID</th>
								<th>FRC Company Name</th>
								<th>User Type</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${companyList}" var="dto">
								<tr>
									<spring:url value="updateUser?userId=${dto.userId}" var="updateUser_url"></spring:url>
									<td><a href="${updateUser_url}">${dto.userId}</a></td>
									<td>${dto.companyName}</td>
									<c:choose>
										<c:when test="${dto.roleType} == 'TARION_ADMIN'">
											<td>Administrator</td>
										</c:when>
										<c:otherwise>
											<td>FRC</td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>	
	</div>
</div>