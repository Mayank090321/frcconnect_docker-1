<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="standard-div">
	<div class="eight-hundred-width">
		<div class="fifty-margin" id="update-user">
			<h2 class="text-align">Edit FRC Connect User</h2>
			<p class="text-align">Please fill out the following information. (Note: All fields are mandatory.)</p>
			
			<spring:url value="update" var="update_url"></spring:url>
			<form method="post" name="updateForm" id="updateForm" action="${update_url}">
				<div class="twenty-margin">
					<h3 class="text-align">General Information</h3>
					<br />
					<input class="form-control" type="hidden" name="userType" value="${userType}" />
					<c:choose>
						<c:when test="${userType == 'frcUser'}">
						<div class="row">
							<label class="form-label">Company Name:</label>
							<input class="form-control" type="text" name="companyName" id="companyAutoComplete" value="${userProfileDto.companyName}" disabled />
							<input class="form-control" type="hidden" name="companyId" id="companyAutoCompleteId" value="${userProfileDto.companyId}"/>
						</div>
						</c:when>
						<c:otherwise>
							<input class="form-control" type="hidden" name="companyName" value="" />
							<input class="form-control" type="hidden" name="companyId" value="" />
						</c:otherwise>
					</c:choose>
					<div class="row">
						<label class="form-label">User ID:</label> 
						<input class="form-control" name="userIdDisplay" value="${userProfileDto.userId}" disabled /> 
						<input class="form-control" type="hidden" name="userId" value="${userProfileDto.userId}"  /> 
					</div>
					<div class="row">
						<label class="form-label">User First Name:</label>
						<input class="form-control" name="userFirstName" value="${userProfileDto.userFirstName}" disabled />
					</div>
					<div class="row">
						<label class="form-label">User Last Name:</label>
						<input class="form-control" name="userLastName" value="${userProfileDto.userLastName}" disabled />
					</div>
					<div class="row">
						<label class="form-label">Email:</label> 
						<input class="form-control" userInputClass" data-errorSpan="emailErrorMessage" name="email" id="email" value="${userProfileDto.email}" />
						<span class="emailErrorMessage form-label">Please Insert Email Address</span>
					</div>
				</div>
	
				
				<div class="twenty-margin">
					<h3 class="text-align">Password Set-Up</h3>
					<br>
					<div class="row">
						<label class="form-label">New Password:</label> 
						<input class="form-control userInputClass" data-errorSpan="passwordErrorMessage" type="password" name="password" id="password" /> 
						<span class="passwordErrorMessage form-label">Please Insert Password</span>
					</div>
					<div class="row">
						<label class="form-label">Confirm Password:</label> 
						<input class="form-control userInputClass" data-errorSpan="passwordConfirmErrorMessage" type="password" name="passwordConfirm" id="passwordConfirm" />
						<span class="passwordConfirmErrorMessage form-label">Passwords do not match</span>
					 </div>
					 <div class="checked-row">
					 	<input class="" type="checkbox" name="passwordChangeRequired" value="true" checked="checked" /> 
						<label class="text-align">Change Password on next Login.</label>
					</div>
					 
					 <div class="row-buttons">
						<spring:url value="searchCompany" var="searchCompany_url"></spring:url>	
						<a href="#" class="btn btn-primary float-right">Save</a>
						<button type="button" class="btn btn-danger open-delete-modal float-right">Delete User</button>
						<a href="${searchCompany_url}" class="btn btn-default">Cancel</a>
					 </div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="deleteModalLabel">Attention!</h4>
	  </div>
	  <div class="modal-body">
		  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You are about to delete the user ${userProfileDto.userId}</p>
		  <p>Do you wish to delete this user?</p>    
		  <spring:url value="delete" var="delete_url"></spring:url>
		  <form method="post" id="deleteForm" name="delete" action="${delete_url}">
				<input type="hidden" name="userId" value="${userProfileDto.userId}" /> 
		  </form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
		<button type="button" class="btn btn-danger">Delete</button>
	  </div>
	</div>
  </div>
</div>

<script>
  $(document).ready(
	function() {
	$('button.open-delete-modal').on("click", function() {
		$("#deleteModal").modal({backdrop: "static", keyboard: false});
	});
	$('#deleteModal').on("show.bs.modal", function(e) {
		$('#deleteModal button.btn-danger').on("click", function() {
			$('#deleteForm').submit();
		})
	});
	$(".userInputClass").keypress(function() {
		$("."+$(this).attr("data-errorSpan")).hide();
	});
	$('#updateForm a.btn-primary').on("click", function() {
		var errorOccured = 0;
		if ($("#email").val() == "") {
			$('.emailErrorMessage').show();
			errorOccured = 1;
		}  else {
			$('.emailErrorMessage').hide();
		}
		if ($("#password").val() == "") {
			$('.passwordErrorMessage').show();
			errorOccured = 1;
		}  else {
			$('.passwordErrorMessage').hide();
		}
		if ($("#passwordConfirm").val() != $("#password").val() ) {
			$('.passwordConfirmErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.passwordConfirmErrorMessage').hide();
		}

		if (errorOccured == 0) {
			$('#updateForm').submit();
		}
	});
  });
</script>

