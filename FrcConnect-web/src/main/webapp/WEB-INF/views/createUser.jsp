<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="standard-div">
	<div class="eight-hundred-width">
		<div class="fifty-margin" id="create-user">
			<h2 class="text-align">Create New FRC Connect User</h2>
			<p class="text-align">Please fill out the following information. (Note: All fields are mandatory.)</p>
			
			<spring:url value="create" var="create_url"></spring:url>
			<form id="createForm" name="createForm" method="post" action="${create_url}">
				<div class="twenty-margin">
					<h3 class="text-align">General Information</h3>
					<br />
					<input class="form-control" type="hidden" name="userType" id="userType" value="${userType}" />
					<c:choose>
						<c:when test="${userType == 'frcUser'}">
						<div class="row">
							<label class="form-label">Company Name:</label>
							<input class="form-control userInputClass" data-errorSpan="companyErrorMessage" type="text" name="companyName" id="companyAutoComplete" />
							<span class="companyErrorMessage form-label">Please Select Company</span>
							<input class="form-control" type="hidden" name="companyId" id="companyAutoCompleteId" value="" />
						</div>
						</c:when>
						<c:otherwise>
							<input class="form-control" type="hidden" name="companyName" value="" />
							<input class="form-control" type="hidden" name="companyId" value="" />
						</c:otherwise>
					</c:choose>
					<div class="row">
						<label class="form-label">User ID:</label> 
						<input class="form-control userInputClass" data-errorSpan="userIdErrorMessage" name="userId" id="userId" /> 
						<span class="userIdErrorMessage form-label">Please Insert User ID</span>
					</div>
					<div class="row">
						<label class="form-label">User First Name:</label>
						<input class="form-control userInputClass" data-errorSpan="userFirstNameErrorMessage" name="userFirstName" id="userFirstName" />
						<span class="userFirstNameErrorMessage form-label">Please Insert First Name</span>
					</div>
					<div class="row">
						<label class="form-label">User Last Name:</label>
						<input class="form-control userInputClass" data-errorSpan="userLastNameErrorMessage" name="userLastName" id="userLastName" />
						<span class="userLastNameErrorMessage form-label">Please Insert Last Name</span>
					</div>
					<div class="row">
						<label class="form-label">Email:</label> 
						<input class="form-control userInputClass" data-errorSpan="emailErrorMessage" name="email" id="email" />
						<span class="emailErrorMessage form-label">Please Insert Email Address</span>
					</div>
				</div>
				
				<div class="twenty-margin">
					<h3 class="text-align">Password Set-Up</h3>
					<br />
					<div class="row">
						<label class="form-label">New Password:</label> 
						<input class="form-control userInputClass" data-errorSpan="passwordErrorMessage" type="password" name="password" id="password" /> 
						<span class="passwordErrorMessage form-label">Please Insert Password</span>
					</div>
					<div class="row">
						<label class="form-label">Confirm Password:</label> 
						<input class="form-control userInputClass" data-errorSpan="passwordConfirmErrorMessage" type="password" name="passwordConfirm" id="passwordConfirm" />
						<span class="passwordConfirmErrorMessage form-label">Passwords do not match</span>
					 </div>
					 <div class="checked-row">
						<input class="" type="checkbox" name="passwordChangeRequired" value="true" checked="checked" /> 
						<label class="text-align">Change Password on next Login.</label>
					</div>
				</div>
				
				<div class="row-buttons">
					<spring:url value="searchCompany" var="searchCompany_url"></spring:url>
					<a href="#" class="btn btn-primary float-right" id="save-create-user">Save</a>
					<a href="${searchCompany_url}" class="btn btn-default">Cancel</a>
				 </div>
			</form>
		</div>
	</div>
</div>

<script>
  $(document).ready(function() {
	$('#companyAutoComplete').autocomplete({
		<spring:url value="getCompaniesLike" var="getCompaniesLike_url"></spring:url>
		serviceUrl: '${getCompaniesLike_url}',
		paramName: "companyNameLike",
		delimiter: ",",
	    transformResult: function(response) {
		return {      	
		  //must convert json to javascript object before process
		  suggestions: $.map($.parseJSON(response), function(item) {
		      return { value: item.value, data: item.name };
		   })
		 };
	    	 },
		onSelect: function (suggestion) {
	        $('#companyAutoCompleteId').val(suggestion.data);
	    }
	 });
	 
	$(".userInputClass").keypress(function() {
		$("."+$(this).attr("data-errorSpan")).hide();
	});
	
	$("#save-create-user").on("click", function() {
		var errorOccured = 0;
		if ($("#userType").val() == "frcUser" && $("#companyAutoCompleteId").val() == "" ) {
			$('.companyErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.companyErrorMessage').hide();
		}
		if ($("#userId").val() == "") {
			$('.userIdErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.userIdErrorMessage').hide();
		}
		if ($("#userFirstName").val() == "") {
			$('.userFirstNameErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.userFirstNameErrorMessage').hide();
		}
		if ($("#userLastName").val() == "") {
			$('.userLastNameErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.userLastNameErrorMessage').hide();
		}
		if ($("#email").val() == "") {
			$('.emailErrorMessage').show();
			errorOccured = 1;
		}  else {
			$('.emailErrorMessage').hide();
		}
		if ($("#password").val() == "") {
			$('.passwordErrorMessage').show();
			errorOccured = 1;
		}  else {
			$('.passwordErrorMessage').hide();
		}
		if ($("#passwordConfirm").val() != $("#password").val() ) {
			$('.passwordConfirmErrorMessage').show();
			errorOccured = 1;
		} else {
			$('.passwordConfirmErrorMessage').hide();
		}

		if (errorOccured == 0) {
			$('#createForm').submit();
		}
	});
	
  });
</script>
