<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="standard-div">
	<div id="loginFrame">
		<h2>Login</h2>
		<br>
		<spring:url value="/resources/j_spring_security_check" var="login_url"></spring:url>
		<form name="f" action="${login_url}" method="POST">
			<div class="row">
				<div class="input-group">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-user" />
					</div>
					<input class="form-control username" type="text" placeholder="Username" name="j_username" />
				</div>
			</div>
			<div class="row">
				<div class="input-group">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-lock" />
					</div>
					<input class="form-control password" type="password" placeholder="Password" name="j_password" />
				</div>
			</div>
			<div class="row">
				<div class=" " id="loginMessage">
				</div>
			</div>
			<br>
			<div class="row">
					<button id="loginButton" type="submit" class="btn btn-primary float-right">Login</button>
			</div>
			<c:if test="${authError == true}">
				<div class="row">
					<br>
					<span class="authError red-text">Invalid Authentication Details</span>
				</div>
			</c:if>
			<div class="row">
				<br />
				<spring:url value="/forgotPassword" var="forgotPassword_url"></spring:url>
				<a href="${forgotPassword_url}">Forgot Password?</a>
			</div>
		</form>
	</div>
</div>