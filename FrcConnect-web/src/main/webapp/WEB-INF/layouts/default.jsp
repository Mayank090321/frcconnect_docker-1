<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<spring:url value="/img/favicon.ico" var="favicon_url" />
<link rel="icon" href="${favicon_url}" />

<spring:url value="/css/jquery-ui-datepicker.min.css" var="jquery_ui_css_url" />
<spring:url value="/css/bootstrap.css" var="bootstrap_css_url" />
<spring:url value="/css/bootstrap-theme.css" var="bootstrap_theme_css_url" />
<spring:url value="/css/frcConnect.css" var="frcConnect_css_url" />
<spring:url value="/css/dataTables.min.css" var="datatables_css_url" />

<link rel="stylesheet" type="text/css" media="screen" href="${jquery_ui_css_url}"></link>
<link rel="stylesheet" type="text/css" media="screen" href="${bootstrap_css_url}"></link>
<link rel="stylesheet" type="text/css" media="screen" href="${bootstrap_theme_css_url}"></link>
<link rel="stylesheet" type="text/css" media="screen" href="${frcConnect_css_url}"></link>
<link rel="stylesheet" type="text/css" media="screen" href="${datatables_css_url}"></link>

<!-- javascript -->
<spring:url value="/js/jquery-3.1.1.min.js" var="jquery_url" />
<spring:url value="/js/bootstrap.min.js" var="bootstrap_url" />
<spring:url value="/js/jquery-ui-datepicker.min.js" var="jquery_ui_url" />
<spring:url value="/js/jquery.autocomplete.min.js" var="jquery_autocomplete_url" />
<spring:url value="/js/jquery.dataTables.min.js" var="jquery_datatables_url" />
<spring:url value="/js/frc.js" var="frc_url" />
<spring:url value="/js/dataTables.bootstrap.min.js" var="dataTables_bootstrap_js_url" />
<spring:url value="/js/jquery.dataTables.min.js" var="jquery_dataTables_js_url" />
		

<script src="${jquery_url}" type="text/javascript"></script>
<script src="${bootstrap_url}" type="text/javascript"></script>
<script src="${jquery_ui_url}" type="text/javascript"></script>
<script src="${jquery_autocomplete_url}" type="text/javascript"></script>
<script src="${jquery_datatables_url}" type="text/javascript"></script>
<script src="${frc_url}" type="text/javascript"></script>
<script src="${jquery_dataTables_js_url}"></script>
<script src="${dataTables_bootstrap_js_url}"></script>

<title>Frc Connect</title>
</head>

<body class="Site">	
		<header class="Site-header">
			<tiles:insertAttribute name="header" ignore="true" />
		</header>
		<div class="Site-content">
			<tiles:insertAttribute name="body" />
		</div>
		<footer class="Site-footer">
			<tiles:insertAttribute name="footer" ignore="true" />
		</footer>
</body>
</html>