/* 
 * 
 * FrcUserProfileEntity.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represents the FRC Site (Enrolment) Entity from TWC_SITE_FRC_VW CRM view
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-19
 * @version 1.0
 */
@Entity
@Table(name = "TWC_SITE_FRC_VW")
public class FrcCrmSiteEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SITE_ID")
    private String siteId;

    @Column(name = "FRC_ID")
    private String companyId;
    @Column(name = "FRC_NAME")
    private String companyName;

    @Column(name = "VB_NUMBER")
    private String vbNumber;
    
    @Column(name = "VB_NAME")
    private String vbName;
    
    @Column(name = "SITE_ADDR1")
    private String siteAddress1;

    @Column(name = "SITE_ADDR2")
    private String siteAddress2;
    @Column(name = "SITE_ADDR3")
    private String siteAddress3;
    @Column(name = "SITE_ADDR4")
    private String siteAddress4;
    
    @Column(name = "SITE_CITY")
    private String city;
    
    @Column(name = "SITE_POSTAL")
    private String postalCode;
    
    @Column(name = "PROJECT_NAME")
    private String projectName;
    @Column(name = "PROJECT_DESCR")
    private String projectDescription;
    @Column(name = "PROJECT_PHASE")
    private String projectPhase;
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getVbNumber() {
		return vbNumber;
	}
	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}
	public String getVbName() {
		return vbName;
	}
	public void setVbName(String vbName) {
		this.vbName = vbName;
	}
	public String getSiteAddress1() {
		return siteAddress1;
	}
	public void setSiteAddress1(String siteAddress1) {
		this.siteAddress1 = siteAddress1;
	}
	public String getSiteAddress2() {
		return siteAddress2;
	}
	public void setSiteAddress2(String siteAddress2) {
		this.siteAddress2 = siteAddress2;
	}
	public String getSiteAddress3() {
		return siteAddress3;
	}
	public void setSiteAddress3(String siteAddress3) {
		this.siteAddress3 = siteAddress3;
	}
	public String getSiteAddress4() {
		return siteAddress4;
	}
	public void setSiteAddress4(String siteAddress4) {
		this.siteAddress4 = siteAddress4;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getProjectPhase() {
		return projectPhase;
	}
	public void setProjectPhase(String projectPhase) {
		this.projectPhase = projectPhase;
	}


    



    

}
