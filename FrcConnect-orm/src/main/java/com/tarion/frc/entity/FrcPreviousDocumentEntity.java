package com.tarion.frc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represents the previous document
 *
 * @author <A href="joni.paananen@tarion.com">Joni Paananen</A>
 * @since 2016-12-10
 * @version 1.0
 */

@Table(name = "TWC_SITE_B19_LIST_VW")
@Entity
public class FrcPreviousDocumentEntity {
	
	@Column(name = "SITE_ID")
	private String siteId;
	@Id
	@Column(name = "SEQNUM")
	private Integer seqNum;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "RECEIVED_DATE")
	private String receivedDate;
	@Column(name = "DECISION_DATE")
	private String decisionDate;
	@Column(name = "DECISION")
	private String decision;
	@Column(name = "ONTIME")
	private String onTime;
	
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}	
	public Integer getSeqNum() {
		return seqNum;
	}
	public void setSeqNum(Integer seqNum) {
		this.seqNum = seqNum;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}
	public String getDecisionDate() {
		return decisionDate;
	}
	public void setDecisionDate(String decisionDate) {
		this.decisionDate = decisionDate;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public String getOnTime() {
		return onTime;
	}
	public void setOnTime(String onTime) {
		this.onTime = onTime;
	}
	
	
	

}
