/* 
 * 
 * FrcCrmB19TemplateTypeEntity.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represents the code / name value pairs
 * of B19 Template types
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-03-08
 * @version 1.0
 */
@Entity
@Table(name = "TWC_B19_DOC_VW")
public class FrcCrmB19TemplateTypeEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "DOC_TYPE_CD")
    private String tempateTypeCode;

    @Column(name = "DOC_TYPE_DESCR")
    private String templateType;

	public String getTempateTypeCode() {
		return tempateTypeCode;
	}

	public void setTempateTypeCode(String tempateTypeCode) {
		this.tempateTypeCode = tempateTypeCode;
	}

	public String getTemplateType() {
		return templateType;
	}

	public void setTemplateType(String templateType) {
		this.templateType = templateType;
	}
}
