/* 
 * 
 * FrcUserProfileEntity.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * This class represents the FRC User Profile.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-09
 * @version 1.0
 */
@Entity
@Table(name = "FRC_USER_PROFILE")
public class FrcUserProfileEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "USER_NAME")
    private String userName;
    @Version
    @Column
    private long version;

    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "FIRST_NAME")
    private String firstName;
    
    @Column(name = "LAST_NAME")
    private String lastName;
    
    @Column(name = "COMPANY_ID")
    private String companyId;
    
    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "CREATE_DTTM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "CREATE_USER_ID")
    private String createUserId;

    @Column(name = "LAST_UPDATE_DTTM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdateDt;

    @Column(name = "LAST_UPDATE_USER_ID")
    private String lastUpdateUserId;
    
    @Column(name = "EXPIRY_DTTM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDt;

    @Column(name = "LAST_LOGIN_DTTM")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLoginDt;

    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     *
     * @param userName the new user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	/**
     * Gets the creates the date.
     *
     * @return the creates the date
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * Sets the creates the date.
     *
     * @param createDate the new creates the date
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * Gets the creates the user id.
     *
     * @return the creates the user id
     */
    public String getCreateUserId() {
        return createUserId;
    }

    /**
     * Sets the creates the user id.
     *
     * @param createUserId the new creates the user id
     */
    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    /**
     * Gets the last update dt.
     *
     * @return the last update dt
     */
    public Date getLastUpdateDt() {
        return lastUpdateDt;
    }

    /**
     * Sets the last update dt.
     *
     * @param lastUpdateDt the new last update dt
     */
    public void setLastUpdateDt(Date lastUpdateDt) {
        this.lastUpdateDt = lastUpdateDt;
    }

    /**
     * Gets the last update user id.
     *
     * @return the last update user id
     */
    public String getLastUpdateUserId() {
        return lastUpdateUserId;
    }

    /**
     * Sets the last update user id.
     *
     * @param lastUpdateUserId the new last update user id
     */
    public void setLastUpdateUserId(String lastUpdateUserId) {
        this.lastUpdateUserId = lastUpdateUserId;
    }
    
    
   	public Date getExpiryDt() {
		return expiryDt;
	}

	public void setExpiryDt(Date expiryDt) {
		this.expiryDt = expiryDt;
	}
	
	public Date getLastLoginDt() {
		return lastLoginDt;
	}

	public void setLastLoginDt(final Date lastLoginDt) {
		this.lastLoginDt = lastLoginDt;
	}

}
