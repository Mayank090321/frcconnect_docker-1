package com.tarion.frc.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.tarion.frc.content.dto.ContentPageType;
import com.tarion.frc.content.dto.ContentTopicType;

/**
 * This class represents the FRC Content Entity
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-03-28
 * @version 1.0
 */
@Entity
@Table(name = "FRC_CONTENT")
public class FrcContentEntity implements Serializable{
	
	private static final long serialVersionUID = 7620085013613379108L;
	
	@Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "PAGE_TYPE")
	@Enumerated(EnumType.STRING)
	private ContentPageType pageType;
	
	@Column(name = "TOPIC_TYPE")
	@Enumerated(EnumType.STRING)
	private ContentTopicType topicType;
	
	@Column(name = "TOPIC")
	private String topic;
	
	@Column(name = "CONTENT_STRING")
	private String content;
	
	@Column(name = "PUBLISH_DTTM")
    @Temporal(TemporalType.TIMESTAMP)
	private Date publishDate;
	
	@Column(name = "CREATED_DTTM")
    @Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ContentPageType getPageType() {
		return pageType;
	}
	public void setPageType(ContentPageType pageType) {
		this.pageType = pageType;
	}
	public ContentTopicType getTopicType() {
		return topicType;
	}
	public void setTopicType(ContentTopicType topicType) {
		this.topicType = topicType;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
