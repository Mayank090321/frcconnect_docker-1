/* 
 * 
 * FrcCrmSiteSixtyDayReportEntity.java
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This class represents the FRC 60 Day Report current Report number for
 * Site (Enrolment) from SITE_60_DAY_REPORT_VW CRM view
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-03-17
 * @version 1.0
 */
@Entity
@Table(name = "SITE_60_DAY_REPORT_VW")
public class FrcCrmSiteSixtyDayReportEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SITE_ID")
    private String siteId;

    @Column(name = "REPORT_NAME")
    private String reportName;
    @Column(name = "REPORT_NUM")
    private String reportNumber;

	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getReportNumber() {
		return reportNumber;
	}
	public void setReportNumber(String reportNumber) {
		this.reportNumber = reportNumber;
	}
}
