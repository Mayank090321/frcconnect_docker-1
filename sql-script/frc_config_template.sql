/*
 This script is maintained in SVN.
 It contains template for complete content of BSA_CONFIG table
 Before running it, insert value definitions for desired environment, below 
 to replace comment that says "TODO Insert replacement values from suitable environment file." 
*/

DELETE FROM [dbo].[FRC_CONFIG]
		
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

PRINT(N'Add 13 rows to [dbo].[FRC_CONFIG]')
GO


/*

TODO Insert replacement values from suitable environment file.

*/

INSERT INTO [dbo].[FRC_CONFIG] ([NAME], [DESCRIPTION], [VALUE]) VALUES 
('frc.ldap.base', 'FRC Ldap User Base DN', @param_ldap_base),
('frc.ldap.initial_context_factory','FRC Ldap InitialContext Factory Class','com.sun.jndi.ldap.LdapCtxFactory'),
('frc.ldap.provider_url','FRC Ldap Provider Url',@param_ldap_server),
('frc.ldap.security_authentication','FRC Ldap Security Authentication Type','simple'),
('frc.ldap.user_dn_template','FRC LDAP User DN Template',@param_ldap_template),
('frc.ldap.weblogic.ejb.url','FRC LDAP Weblogic EJB URL',@param_ldap_weblogic),
('frc.ws.client.esp_email_sender.wsdl.url','FRC ESP Emailing Web Service WSDL Url','http://' + @param_tip_server + '/EmailSenderWSBean/EmailSender?WSDL'),
('frc.email.body.new_user','FRC Email New User Body Pattern','Dear User, <p>Please find your log in information for FRC Connect below:</p><p>Your User ID: {0}<br/>Your temporary password: {1}</p><p>To activate your access, click on the following link: <a href="{2}">Sign in to FRC Connect</A> </p> '),
('frc.email.subject.new_user','FRC Email New User Subject','New FRC Connect User Registration'),
('frc.email.frc_connect_app.url','FRC Connect App base url','http://' + @param_frc_server + '/frc'),
('frc.email.subject.user_password_changed','FRC Email User Password Changed Subject','FRC Connect User Password Changed'),
('frc.email.body.user_password_changed','FRC Email User Password Changed Body Pattern','Dear User, <p>Please find your log in information for FRC Connect below:</p><p>Your User ID: {0}<br/>Your new password: {1}</p><p>To activate your access, click on the following link: <a href="{2}">Sign in to FRC Connect</A> </p> '),
('frc.ws.client.tbi_upload_attachments.wsdl.url','FRC TBI Attachments Upload Web Service WSDL Url','http://' + @param_tip_server + '/AttachmentsWSBean/AttachmentsWSBeanService?WSDL')

COMMIT TRANSACTION
GO