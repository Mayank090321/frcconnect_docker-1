USE [FRC_DEV1]
GO
			
INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ldap.base'
           ,'FRC Ldap User Base DN'
           ,'ou=users,dc=frcdev1,dc=tarion,dc=com')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ldap.initial_context_factory'
           ,'FRC Ldap InitialContext Factory Class'
           ,'com.sun.jndi.ldap.LdapCtxFactory')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ldap.provider_url'
           ,'FRC Ldap Provider Url'
           ,'ldap://ldapdev.tarion.com:1389')
GO


INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ldap.security_authentication'
           ,'FRC Ldap Security Authentication Type'
           ,'simple')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ldap.user_dn_template'
           ,'FRC LDAP User DN Template'
           ,'uid={0},ou=users,dc=frcdev1,dc=tarion,dc=com')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ldap.weblogic.ejb.url'
           ,'FRC LDAP Weblogic EJB URL'
           ,'t3://DEV1JAVAPP01:10190')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ws.client.esp_email_sender.wsdl.url'
           ,'FRC ESP Emailing Web Service WSDL Url'
           ,'http://tipdev1.tarion.com/EmailSenderWSBean/EmailSender?WSDL')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.email.body.new_user'
           ,'FRC Email New User Body Pattern'
           ,'Dear User, <p>Please find your log in information for FRC Connect below:</p><p>Your User ID: {0}<br/>Your temporary password: {1}</p><p>To activate your access, click on the following link: <a href="{2}">Sign in to FRC Connect</A> </p> ')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.email.subject.new_user'
           ,'FRC Email New User Subject'
           ,'New FRC Connect User Registration')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.email.frc_connect_app.url'
           ,'FRC Connect App base url'
           ,'http://frcdev1.tarion.com/frc')
GO


INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.email.subject.user_password_changed'
           ,'FRC Email User Password Changed Subject'
           ,'FRC Connect User Password Changed')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.email.body.user_password_changed'
           ,'FRC Email User Password Changed Body Pattern'
           ,'Dear User, <p>Please find your log in information for FRC Connect below:</p><p>Your User ID: {0}<br/>Your new password: {1}</p><p>To activate your access, click on the following link: <a href="{2}">Sign in to FRC Connect</A> </p> ')
GO

INSERT INTO [dbo].[FRC_CONFIG]
           ([NAME]
           ,[DESCRIPTION]
           ,[VALUE])
     VALUES
           ('frc.ws.client.tbi_upload_attachments.wsdl.url'
           ,'FRC TBI Attachments Upload Web Service WSDL Url'
           ,'http://tipdev1.tarion.com/AttachmentsWSBean/AttachmentsWSBeanService?WSDL')
GO


