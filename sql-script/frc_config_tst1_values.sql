/*
This script contains values for FRC_CONFIG table for DEV1 environment.
Insert it into frc_config_template.sql where there is a comment that says "TODO Insert replacement values from suitable environment file."
*/
declare @param_ldap_base varchar(2084)
set @param_ldap_base = 'ou=users,dc=frctst1,dc=tarion,dc=com'

declare @param_ldap_template varchar(2084)
set @param_ldap_template = 'uid={0},ou=users,dc=frctst1,dc=tarion,dc=com'

declare @param_ldap_server varchar(2084)
set @param_ldap_server = 'ldap://ldapdev.tarion.com:1389'

declare @param_ldap_weblogic varchar(2084)
set @param_ldap_weblogic = 't3://TST1JAVAPP01:10190,TST1JAVAPP02:10190'

declare @param_tip_server varchar(2084)
set @param_tip_server = 'tiptst1.tarion.com'

declare @param_frc_server varchar(2084)
set @param_frc_server = 'frctst1.tarion.com'
