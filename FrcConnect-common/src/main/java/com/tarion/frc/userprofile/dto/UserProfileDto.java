/* 
 * 
 * UserProfileDto.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.userprofile.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * This class holds all the user-related data.
 *
 * @author <A href="paul.richardson@tarion.com">Paul Richardson</A>
 * @since 2012-06-26
 * @version 1.0
 */
public class UserProfileDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3728003757348855328L;

    /**
     * The Enum RoleType.
     *
     * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
     * @since 13-Sep-2012
     * @version 1.0
     */
    public enum RoleType {
        TARION_ADMIN, FRC_USER
    }

    // @Validate("maxLength=30")
    private String userId;
    // @Validate("maxLength=30")
    private String email;
    
    private String companyName;
    private String companyId;
    
    private String userFirstName;
    private String userLastName;

    private boolean accountLocked;
    private boolean passwordChangeRequired;
    private boolean firstTimeLogin;
    private RoleType roleType;
    private Date expiryDate;
    
    // proxy user stuff
    private boolean proxyUserActivated;
    private String originalUserId;

    /**
     * Instantiates a new user profile dto.
     */
    public UserProfileDto() {
    }
    
    /**
     * Gets the user id.
     *
     * @return the user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the user id.
     *
     * @param userId the new user id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	/**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Checks if is account locked.
     *
     * @return true, if is account locked
     */
    public boolean isAccountLocked() {
        return accountLocked;
    }

    /**
     * Sets the account locked.
     *
     * @param accountLocked the new account locked
     */
    public void setAccountLocked(boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    /**
     * Checks if is password change required.
     *
     * @return true, if is password change required
     */
    public boolean isPasswordChangeRequired() {
        return passwordChangeRequired;
    }

    /**
     * Sets the password change required.
     *
     * @param passwordChangeRequired the new password change required
     */
    public void setPasswordChangeRequired(boolean passwordChangeRequired) {
        this.passwordChangeRequired = passwordChangeRequired;
    }
    
    /**
     * Checks if it is the user's first time logging-in.
     * @return true, if is the user's first time logging-in
     */
    public boolean isFirstTimeLogin() {
		return firstTimeLogin;
	}

    /**
     * Sets the user's first time logging-in.
     *
     * @param firstTimeLogin the user's first time logging-in
     */
	public void setFirstTimeLogin(final boolean firstTimeLogin) {
		this.firstTimeLogin = firstTimeLogin;
	}

	/**
     * Gets the role type.
     *
     * @return the role type
     */
    public RoleType getRoleType() {
        return roleType;
    }

    /**
     * Sets the role type.
     *
     * @param roleType the new role type
     */
    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

	/**
     * Checks if is proxy user activated.
     *
     * @return true, if is proxy user activated
     */
    public boolean isProxyUserActivated() {
        return proxyUserActivated;
    }

    /**
     * Sets the proxy user activated.
     *
     * @param proxyUserActivated the new proxy user activated
     */
    public void setProxyUserActivated(boolean proxyUserActivated) {
        this.proxyUserActivated = proxyUserActivated;
    }

    /**
     * Gets the original user id.
     *
     * @return the original user id
     */
    public String getOriginalUserId() {
        return originalUserId;
    }

    /**
     * Sets the original user id.
     *
     * @param originalUserId the new original user id
     */
    public void setOriginalUserId(String originalUserId) {
        this.originalUserId = originalUserId;
    }

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
    	ToStringBuilder builder = new ToStringBuilder(this,ToStringStyle.SIMPLE_STYLE);
    	builder.append("userId",userId);
    	builder.append("email", email);
    	    	
    	return builder.build();
    }

	public boolean getTarionAdminRole() {
		return roleType == RoleType.TARION_ADMIN;
	}
}
