/* 
 * 
 * BsaUtil.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tarion.frc.common.constants.FrcConstants;
import com.tarion.frc.common.exceptions.FrcGeneralException;

/**
 * This is a utility class to handle small common tasks.
 * 
 * @author <A href="paul.richardson@tarion.com">Paul Richardson</A>
 * @since 2012-07-05
 * @version 1.0
 */
public class FrcUtil {

	private static final String H_FOR_HOMEOWNER = "H";
	private static final String B_FOR_BUILDER = "B";

	private static final Logger logger = LoggerFactory.getLogger(FrcUtil.class);
	
	public static Date convertCrmStringDateToDate(String crmStringDate) {
		DateFormat format = new SimpleDateFormat(FrcConstants.CRM_DATE_FORMAT);
		Date crmDate = null;
		try {
			crmDate = format.parse(crmStringDate);
		} catch (Exception e) {
			// Crm date is in invalid format
		}
		return crmDate;
	}
	
	public static String trimCrmStringDate(String crmStringDate) {
		Date crmDate = convertCrmStringDateToDate(crmStringDate);
		DateFormat format = new SimpleDateFormat(FrcConstants.CRM_DATE_FORMAT);		
		return format.format(crmDate);
	}
	
	public static String todayFormattedForCm() {
		DateFormat format = new SimpleDateFormat(FrcConstants.CRM_DATE_FORMAT);
		return format.format(new java.util.Date());
	}
	
	/**
	 * To format the date.
	 *
	 * @param theDate the the date
	 * @return the formatted date
	 */
	public static String getFormattedDate(Date theDate) {
    	try {
	    	DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	    	return df.format(theDate);
    	} catch (java.lang.Exception e) {
    		return null;
    	}
    }

	/**
	 * To format the date.
	 *
	 * @param theDate the the date
	 * @return the formatted date
	 */
	public static String getFormattedDateForEmail(Date theDate) {
    	try {
	    	DateFormat df = new SimpleDateFormat("MMMM d, yyyy");
	    	return df.format(theDate);
    	} catch (java.lang.Exception e) {
    		return null;
    	}
    }

	public static <T> T convertXmlToJaxb(String xmlResponse, Class<T> clazz) throws FrcGeneralException {
		T response = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader stringReader = new StringReader(xmlResponse);
			response = (T) unmarshaller.unmarshal(stringReader);
		} catch (JAXBException e) {
			throw new FrcGeneralException("Error converting xml to JAXB of type: " + clazz, e);
		}

		return response;
	}

	public static <T> String convertJaxbToXml(T jaxb, Class<T> clazz) {
		StringWriter stringWriter = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Marshaller marshaller = jaxbContext.createMarshaller();

			// marshaller.setProperty(Marshaller.JAXB_ENCODING, "ISO-8859-1");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(jaxb, stringWriter);
		} catch (JAXBException e) {
			throw new FrcGeneralException("Error converting JAXB to xml ", e);
		}

		return stringWriter.toString();
	}
	
	/**
	 * Convert Date to String
	 * 
	 * @param date
	 * @param format
	 * @return
	 */
	public static String convertDateToString(Date date, String format){
		if(date != null){
			if(format == null){
				format = FrcConstants.DEFAULT_DATE_FORMAT;
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			return dateFormat.format(date);
		}else{
			return "";
		}
	}
	
	public static String convertDateToString(Date date){
		String format = FrcConstants.DEFAULT_DATE_FORMAT;
		if(date != null){
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			return dateFormat.format(date);
		}else{
			return "";
		}
	}
	
	public static String convertCurrencyDoubletoString(Double number){
		if (number != null) {
			NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.CANADA);
			nf.setMinimumFractionDigits(2);
			return nf.format(number);
		} else {
			return "";
		}	
	}

	/**
	 * Return VB number as Integer
	 * 
	 * @param vbNumber
	 * @return
	 */
	public static Integer getVBNumberAsInteger(String vbNumber){
		if(vbNumber == null || vbNumber.isEmpty()){
			return null;
		}else{
			if(!vbNumber.startsWith(FrcConstants.B_FOR_BUILDER)){
				logger.error("Invalid VB number: " + vbNumber);
				return null;
			}
			
			Integer result = null;		
			try {
				result =  new Integer(vbNumber.substring(1));
			} catch (NumberFormatException e) {
				logger.error("Invalid VB number: " + vbNumber);
			}
			return result;
		}
	}
	


	/**
	 * Utility method to convert java.util.Date to XMLGregorianCalendar used by
	 * WebService client classes
	 * 
	 * @param date
	 *            to convert to XMLGregorianCalendar
	 * @return
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendarFromDate(Date date) {
		if (date == null) {
			return null;
		}
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		XMLGregorianCalendar xmlDate = null;
		try {
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
		} catch (Exception e) {
			// format was incorrect, nothing we can do
		}
		return xmlDate;
	}

	/**
	 * Utility method to convert XMLGregorianCalendar to Date used by tml
	 * controler classes
	 * 
	 * @param calendar
	 *            to convert to Date
	 * @return
	 */
	public static Date getDateFromXMLGregorianCalendar(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	/**
	 * Create an XMLGregorianCalendar for the Current Date/Time
	 * 
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar getXMLGregorianCalendarNow() {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		try {
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
			return now;
		} catch (DatatypeConfigurationException e) {
			return null;
		}
	}

	/**
	 * Get Current Date/Time
	 * 
	 * @return
	 */
	public static Date getDateNow() {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		return gregorianCalendar.getTime();
	}
	
	public static String getDateNowFormatted() {
		Date date = getDateNow();
		SimpleDateFormat dateFormat = new SimpleDateFormat(FrcConstants.DEFAULT_DATE_TIME_FORMAT);
		String dateFormatted = dateFormat.format(date);
		return dateFormatted;
	}

	/**
	 * Check if the string is null or empty.
	 * 
	 * @param aString
	 *            String to test
	 * @return true= null or empty, false otherwise
	 */
	public static boolean isNullOrEmpty(String aString) {
		boolean result = true;
		if (aString != null && aString.trim().length() > 0) {
			result = false;
		}
		return result;
	}

	public static boolean validateEmail(String email) {
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
		Matcher matcher = pattern.matcher(email);
		boolean isMatch = matcher.matches();

		return isMatch;
	}

	/**
	 * Adds the h to enrolment.
	 * 
	 * @param enrolNum
	 *            the enrol num
	 * @return the string
	 */
	public static String addHToEnrolment(String enrolNum) {
		if (enrolNum != null && enrolNum.length() > 1 && (H_FOR_HOMEOWNER.equalsIgnoreCase(enrolNum.substring(0, 1)))) {
			return enrolNum.toUpperCase();
		} else if (enrolNum == null) {
			return enrolNum;
		}
		return H_FOR_HOMEOWNER + enrolNum;
	}

	/**
	 * Adds the b to vb.
	 * 
	 * @param vbNum
	 *            the vb num
	 * @return the string
	 */
	public static String addBToVb(Integer vbNum) {
		if (vbNum != null) {
			return B_FOR_BUILDER + vbNum;
		} else {
			return null;
		}
	}

	/**
	 * Adds the b to vb.
	 * 
	 * @param vbNum
	 *            the vb num
	 * @return the string
	 */
	public static String addBToVb(String vbNum) {
		if (vbNum != null && vbNum.length() > 1 && (B_FOR_BUILDER.equalsIgnoreCase(vbNum.substring(0, 1)))) {
			return vbNum.toUpperCase();
		} else if (vbNum == null) {
			return vbNum;
		}
		return B_FOR_BUILDER + vbNum;
	}

	/**
	 * Removes the h from enrolment.
	 * 
	 * @param enrolNum
	 *            the enrol num
	 * @return the string
	 */
	public static String removeHFromEnrolment(String enrolNum) {
		if (enrolNum != null && enrolNum.length() > 1 && (H_FOR_HOMEOWNER.equalsIgnoreCase(enrolNum.substring(0, 1)))) {
			return enrolNum.toUpperCase().substring(1);
		} else if (enrolNum == null) {
			return enrolNum;
		}
		return enrolNum;
	}

	/**
	 * Removes the b from vb.
	 * 
	 * @param vbNumbder
	 *            the vb numbder
	 * @return the string
	 */
	public static String removeBFromVB(String vbNumbder) {
		if (vbNumbder != null && vbNumbder.length() > 1 && (B_FOR_BUILDER.equalsIgnoreCase(vbNumbder.substring(0, 1)))) {
			return vbNumbder.toUpperCase().substring(1);
		} else if (vbNumbder == null) {
			return vbNumbder;
		}
		return vbNumbder;
	}

	/**
	 * Null safe equals.
	 * 
	 * @param o1
	 *            the o1
	 * @param o2
	 *            the o2
	 * @return true, if successful
	 */
	public static boolean nullSafeEquals(String o1, String o2) {
		if ((o1 == null || "".equals(o1)) && (o2 == null || "".equals(o2))) {
			return true;
		} else if (o1 != null && o2 == null) {
			return false;
		} else if (o2 != null && o1 == null) {
			return false;
		}
		return o1.equals(o2);
	}

	/**
	 * Checks if is numeric.
	 * 
	 * @param str
	 *            the str
	 * @return true, if is numeric
	 */
	public static boolean isNumeric(String str) {
		if (str == null)
			return false;
		try {
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	/**
	 * Close the jdbc connections.
	 * 
	 * @param rs
	 *            Result set
	 * @param ps
	 *            prepared statement
	 * @param con
	 *            Connection
	 */
	public static void closeConnections(ResultSet rs, PreparedStatement ps, Connection con) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
			}
		}
		if (ps != null) {
			try {
				ps.close();
			} catch (Exception e) {
			}
		}
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				// problem during close, don't write out exception.
			}
		}
	}

	/**
	 * Simply takes a string and returns true If and Only If the String is a
	 * positive Integer.
	 * 
	 * @param number
	 *            The number to check
	 * @return True if and only if the string is a number greater than 0
	 */
	public static boolean isNonZero(String number) {
		if (!isNullOrEmpty(number)) {
			try {
				int num = Integer.parseInt(number.trim());
				if (num > 0) {
					return true;
				}
			} catch (Exception e) {
				// Swallow
			}
		}
		return false;
	}


	public static String getCurrencyFormattedString(Double number) {
		if (number != null) {
			NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.CANADA);
			nf.setMinimumFractionDigits(2);
			return nf.format(number);
		} else {
			return "";
		}
	}

	public static String addBPToBatchControlNumber(BigDecimal batchControlNumber) {
		return "BP" + batchControlNumber;
	}
	
	public static String addBPToBatchControlNumber(String batchControlNumber) {
		return "BP" + batchControlNumber;
	}
	
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	public static Boolean isDateValid(Date date) {
		if (date != null) {
			if (date.before(new Date(0l))) {
				return false;
			}
		}
		return true;
	}
	
	public static Double getDoubleFromCurrencyString(String input){
		Double ret = null;
		try{
			ret = Double.valueOf(input.replace("$", "").replace(",", "").replace(" ", ""));
		}catch (NumberFormatException e){
			ret = 0d;
		}catch (NullPointerException e){
			ret = 0d;
		}
		return ret;
	}
	
	/**
	 * UnMarshalls XML string into instance of given class
	 * 
	 * TODO Use generics
	 */
	public static Object unMarshalMsg(String msg, Object object) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(object.getClass());
		Unmarshaller u = jc.createUnmarshaller();
		return u.unmarshal(new StringReader(msg));
	}

	/**
	 * Marshalls given object into XML string
	 * 
	 * TODO Use generics
	 */
	public static String marshalMessage(Object object) throws JAXBException {

		String ret = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(object.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter writer = new StringWriter();
			m.marshal(object, writer);

			ret = writer.toString();
		} catch (Exception e) {
			logger.error("Error unmarshalling: " + object, e);
		}
		return ret;
	}

	/**
	 * Gets the type.
	 *
	 * @param <T> the generic type
	 * @param enumClass the enum class
	 * @param typeCode the type code
	 * @param propertyToFilter the property to filter
	 * @return the type
	 */
	public static <T extends Enum> List<T> getType(Class<? extends T> enumClass, String typeCode, String propertyToFilter) {
		List<T> lst = new ArrayList<T>();		
		String type = null;
		for (T e1 : enumClass.getEnumConstants()) {
			try {
				type = (String) PropertyUtils.getProperty(e1, propertyToFilter);
			} catch(Exception ex) {
			    logger.error("Problem getting Type", ex);
			}
			if (type.contains(typeCode)) {
				lst.add(e1);
			}			
		}
		return lst;
	}	
	
	/**
	 * Returns Enum of the given type, from code value.
	 *
	 * @param <T> the generic type
	 */
	public static <T extends Enum<T>> T getEnumFromCodeValue(Class<T> enumClass, String codeValue) {
		T returnValue = null;
		for (T e1 : enumClass.getEnumConstants()) {
			try {
				String code = (String) PropertyUtils.getProperty(e1, "code");
				if (code != null && code.equals(codeValue)) {
					returnValue = e1;
					break;
				}
			} catch(Exception ex) {
			    logger.error("Problem getting Code Value", ex);
			}
		}		
	    return returnValue;
	}

}