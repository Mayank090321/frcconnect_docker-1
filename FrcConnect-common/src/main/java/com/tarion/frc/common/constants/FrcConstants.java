/* FrcConstants
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 */
package com.tarion.frc.common.constants;

/**
 * Constants used within the Email Service Provider
 * 
 * @author <a href="bojan.volcansek@tarion.com">Bojan Volcansek</a>
 * @date 2016-03-01
 * @version 1.0
 */
public interface FrcConstants {
    public static final String LDAP_CONTEXT = "ldap/frcLdap";
    public static final String FRC_LDAP_BASE = "frc.ldap.base";

	public static final String FRC_WS_CLIENT_TBI_UPLOAD_ATTACHMENTS_WSDL_URL = "frc.ws.client.tbi_upload_attachments.wsdl.url";

	public static final String FRC_WS_CLIENT_ESP_WEB_SERVICE_WSDL_URL = "frc.ws.client.esp_email_sender.wsdl.url"; 
	public static final String FRC_EMAIL_BODY_NEW_USER = "frc.email.body.new_user";
	public static final String FRC_EMAIL_SUBJECT_NEW_USER = "frc.email.subject.new_user";
	public static final String FRC_EMAIL_FRC_CONNECT_APP_URL = "frc.email.frc_connect_app.url";
	public static final String FRC_EMAIL_BODY_USER_PASSWORD_CHANGED = "frc.email.body.user_password_changed";
	public static final String FRC_EMAIL_SUBJECT_USER_PASSWORD_CHANGED = "frc.email.subject.user_password_changed";
	
	public static final long FRC_CONNECT_CM_UPLOAD_SOURCE_SYSTEM = 5l;
	
	public static final String FRC_CM_ATTRIBUTE_NAME_VBNUMBER = "VBNumber";
	public static final String FRC_CM_ATTRIBUTE_NAME_ENROLMENT_NUMBER = "EnrolmentNumber";
	public static final String FRC_CM_ATTRIBUTE_NAME_CREATED_DATE = "CreatedDate";
	public static final String FRC_CM_ATTRIBUTE_NAME_CM_FILE_NAME = "CMFileName";
	public static final String FRC_CM_ATTRIBUTE_NAME_TEMPLATE_TYPE = "TemplateType";
	public static final String FRC_CM_ATTRIBUTE_NAME_TEMPLATE_TYPE_CODE = "TemplateTypeCode";
	public static final String FRC_CM_ATTRIBUTE_NAME_60_DAY_TEMPLATE_TYPE_START_DATE = "StartDate";
	public static final String FRC_CM_ATTRIBUTE_NAME_60_DAY_TEMPLATE_TYPE_END_DATE = "EndDate";
	public static final String FRC_CM_ATTRIBUTE_NAME_60_DAY_TEMPLATE_TYPE_REPORT_NUMBER = "ReportNum";
	public static final String FRC_CM_ATTRIBUTE_NAME_CREATED_BY = "ScannerID";
	public static final String FRC_CM_ATTRIBUTE_NAME_SCAN_DATE = "ScanDate";

	public static final String FRC_CM_ITEM_TYPE_BB19_DOCUMENT = "BB19Document";
	public static final String FRC_CM_TEMPLATE_TYPE_CODE_60_DAY_TEMPLATE = "21";

	
	public static final String Y_YES = "Y";
    public static final String N_NO = "N";
    
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    public static final String CRM_DATE_FORMAT = "yyyy-MM-dd";
    public static final Integer CRM_VB_LICENCE_STATUS_PENDING = 8;
    public static final Integer CRM_VB_LICENCE_STATUS_REGISTERED = 9;
    public static final Integer CRM_VB_LICENCE_STATUS_EXPIRED = 10;

    String FRC_PROPERTIES_FILE = "frc.properties";

	String H_FOR_HOMEOWNER = "H";
	String B_FOR_BUILDER = "B";
	
	public static final String LDAP_GROUP_TARION_ADMIN = "TarionAdmin";
	public static final String LDAP_GROUP_FRC_USER = "FrcUser";
	
	public static final String TARION_ADMIN_COMPANY_ID = "-1";
	public static final String TARION_ADMIN_COMPANY_NAME = "TarionAdmin";


	///////////////////////
	

    /** Constant for the ESP MAil Session JNDI name */
    String MAIL_JNDI = "mail/TIPSession";

    /** Properties files name */
    
    /** Email Sender Properties */
	String EMAIL_WS_CLIENT_TBI_ATTACHMENTS_WSDL_URL = "esp.ws.client.tbi_attachments.wsdl.url";
	public static final String FRC_WS_CLIENT_TBI_FRC_WSDL_URL = "frc.ws.client.tbi_frc.wsdl.url";
	Long EMAIL_TBI_CM_UPLOAD_SOURCE_ID = 3L;

    String EMAIL_FROM_ADDRESS = "esp.default_sender_email";
    String EMAIL_RETRY_NUMBER_OF_TIMES = "esp.number_of_retries";
    
    String EMAIL_DEFAULT_XHTML_PDF_TEMPLATE = "email.to.cm.pattern.default";
    
	String EMAIL_CM_ATTRIBUTE_NAME_VBNUMBER = "VBNumber";
	String EMAIL_CM_ATTRIBUTE_NAME_ENROLMENT_NUMBER = "EnrolmentNumber";
	String EMAIL_CM_ATTRIBUTE_NAME_TEMPLATE_TYPE = "TemplateType";
	String EMAIL_CM_ATTRIBUTE_NAME_CREATED_DATE = "CreatedDate";
	String EMAIL_CM_ATTRIBUTE_NAME_CM_FILE_NAME = "CMFileName";
	String EMAIL_CM_ATTRIBUTE_NAME_CM_FILE_NAME_VALUE = "CrmEmail_";
    
    String EMAIL_ADMIN_USER = "esp.admin";
    
	
	int EMAIL_MAX_ERROR_TEXT = 1024;
	
    /** Constant for the ping message type */
    String PING = "ping";
    /** Constant for the message property for the message type */
    String MESSAGE_TYPE = "MessageType";
    String MESSAGE_NAME = "MessageName";
    String MESSAGE_NAME_EMAIL_SEND_REQUEST = "TWC_EMAIL_SERVICE_REQ.V1";
    /** Constant for the message property for the destination nodes */
    String DEST_NODES = "DestinationNodes";
    
    String JNDISERVICELOCATOR_TIP_URL = "JNDIServiceLocator.tip.url";
    String EXCHANGE_IMPERSONATION_USER = "exchange.impersonation.user";
    String EXCHANGE_IMPERSONATION_PASSWORD = "exchange.impersonation.password";
}
