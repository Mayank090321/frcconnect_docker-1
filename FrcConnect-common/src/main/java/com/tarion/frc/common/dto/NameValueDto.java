package com.tarion.frc.common.dto;

import java.io.Serializable;

public class NameValueDto implements Serializable {
	private static final long serialVersionUID = 3715965036465464688L;
	
	private String name;
	private String value;
	
	public NameValueDto() {
		
	}

	public NameValueDto(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
