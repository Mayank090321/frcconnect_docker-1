/* 
 * 
 * EmailSendingException.java
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.common.exceptions;

import org.apache.commons.lang3.StringUtils;

/**
 * Exception that occurs during Email Sending.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since Dec 18, 2014
 * @version 1.0
 */
public class FrcGeneralException extends RuntimeException {
    
       /** Serial version ID */
       private static final long serialVersionUID = 1L;

       /** Error code */
       protected String errorCode = null;

       /**
        * Instantiates a new exception.
        */
       public FrcGeneralException() {
       }

       /**
        * Instantiates a new exception.
        *
        * @param msg the msg
        */
       public FrcGeneralException(String msg) {
           super(msg);
       }

       /**
        * Instantiates a new exception.
        *
        * @param msg the msg
        * @param errorCode the error code
        */
       public FrcGeneralException(String msg, String errorCode) {
           super(msg);
           this.errorCode = errorCode;
       }

       /**
        * Instantiates a new exception.
        *
        * @param msg the msg
        * @param errorCode the error code
        * @param cause the cause
        */
       public FrcGeneralException(String msg, String errorCode, Throwable cause) {
           super(msg, cause);
           this.setStackTrace(cause.getStackTrace());
           this.errorCode = errorCode;
       }

       /**
        * Instantiates a new exception.
        *
        * @param errorCode the error code
        * @param cause the cause
        */
       public FrcGeneralException(String errorCode, Throwable cause) {
           super(cause);
           this.setStackTrace(cause.getStackTrace());
           this.errorCode = errorCode;
       }

       /**
        * Gets the error code.
        *
        * @return the error code
        */
       public String getErrorCode() {
           return errorCode;
       }

       /**
        * Return the error code number, by first stripping off the beginning value
        * <code>BSA-</code>.
        *
        * @return Error code number (eg. BSA-99999, will return 99999)
        */
       public int getErrorCodeNumber() {
           int errorCodeNumber = 0;
           if (StringUtils.startsWith(errorCode, "BSA-")) {
        	   final String code = StringUtils.substringAfter(errorCode, "BSA-");
               try {
                   errorCodeNumber = Integer.parseInt(code);
               } catch (NumberFormatException nfe) {
                   // Default to negative one if bad error code number
                   errorCodeNumber = -1;
               }
           }
           return errorCodeNumber;
       }

       /* (non-Javadoc)
        * @see java.lang.Throwable#toString()
        */
       @Override
       public String toString() {
           StringBuilder buf = new StringBuilder();
           if (errorCode != null) {
               buf.append(errorCode);
               buf.append(": ");
           }
           buf.append(super.toString());
           return buf.toString();
       }
}