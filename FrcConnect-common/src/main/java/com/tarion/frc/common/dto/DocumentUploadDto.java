/* DocumentUploadDto
 *
 * Copyright (c) 2016 Tarion Warranty Corporation.
 * All rights reserved. 
 */
package com.tarion.frc.common.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * DTO to hold File and its attributes from the UI to the service layer
 * 
 * @author <a href="bojan.volcansek@tarion.com">Bojan Volcansek</a>
 * @date 2016-03-01
 * @version 1.0
 */
public class DocumentUploadDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mimeFileType;
    private String filename;
    private byte[] fileData;
    private NameValueDto templateType;
    private Date startDate;
    private Date endDate;
    private String reportNumber;
    
	public String getMimeFileType() {
		return mimeFileType;
	}
	public void setMimeFileType(String mimeFileType) {
		this.mimeFileType = mimeFileType;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public byte[] getFileData() {
		return fileData;
	}
	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}
	public NameValueDto getTemplateType() {
		return templateType;
	}
	public void setTemplateType(NameValueDto templateType) {
		this.templateType = templateType;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getReportNumber() {
		return reportNumber;
	}
	public void setReportNumber(String reportNumber) {
		this.reportNumber = reportNumber;
	}
	
}
