/* 
 * 
 * TapestryLdapPermissionConstants.java
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.common.constants;

/**
 * LDAP Constatnts - Attribute names as well as Tarion Admin Permission Constant
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 16-Jan-2014
 * @version 1.0
 */
public class FrcLdapConstants {
	
	public static final String TARION_ADMIN = "TarionAdmin";
	
	public static final String USER_ID = "uid";
	
}
