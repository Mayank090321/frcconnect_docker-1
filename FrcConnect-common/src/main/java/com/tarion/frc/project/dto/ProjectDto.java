/* 
 * 
 * UserProfileDto.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.project.dto;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * This class holds all the user-related data.
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2016-02-25
 * @version 1.0
 */
public class ProjectDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5579885037310821255L;

	private String enrolmentNumber;
	private String civicAddress;
	private String vbName;
	private String vbNumber;
	private String phaseNumber;
	private String projectName;

	/**
	 * Instantiates a new project dto.
	 */
	public ProjectDto() {
	}

	public String getEnrolmentNumber() {
		return enrolmentNumber;
	}

	public void setEnrolmentNumber(String enrolmentNumber) {
		this.enrolmentNumber = enrolmentNumber;
	}

	public String getCivicAddress() {
		return civicAddress;
	}

	public void setCivicAddress(String civicAddress) {
		this.civicAddress = civicAddress;
	}

	public String getVbName() {
		return vbName;
	}

	public void setVbName(String vbName) {
		this.vbName = vbName;
	}

	public String getVbNumber() {
		return vbNumber;
	}

	public void setVbNumber(String vbNumber) {
		this.vbNumber = vbNumber;
	}

	public String getPhaseNumber() {
		return phaseNumber;
	}

	public void setPhaseNumber(String phaseNumber) {
		this.phaseNumber = phaseNumber;
	}
	
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE);
		builder.append("enrolmentNumber", enrolmentNumber);
		builder.append("projectName", projectName);
		builder.append("civicAddress", civicAddress);
		builder.append("vbName", vbName);
		builder.append("phaseNumber", phaseNumber);
		return builder.build();
	}

}
