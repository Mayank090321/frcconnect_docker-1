package com.tarion.frc.project.dto;
import java.io.Serializable;
import java.util.*;


/**
 *
 * @author <A href="shadi@dse-solution.com">Shadi Kajevand</A>
 * @since 2016-12-03
 * @version 1.0
 */
public class PreviousDocumentsDto implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 805570971489119564L;
	
	private String description;
	private Date receivedDate; 
	private String decision; 
	private Date decisionDate;
	private String onTime;
	public String getDescription() {
		return description;
	}
	public Date getReceivedDate() {
		return receivedDate;
	}
	public String getDecision() {
		return decision;
	}
	public Date getDecisionDate() {
		return decisionDate;
	}
	public String getOnTime() {
		return onTime;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public void setDecisionDate(Date decisionDate) {
		this.decisionDate = decisionDate;
	}
	public void setOnTime(String onTime) {
		this.onTime = onTime;
	}
	
	
}
