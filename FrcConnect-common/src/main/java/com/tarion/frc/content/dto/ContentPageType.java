package com.tarion.frc.content.dto;

public enum ContentPageType {
	HOME_PAGE("Home Page (After Login)");
	
	private String label;
	private ContentPageType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
}
