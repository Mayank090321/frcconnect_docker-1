package com.tarion.frc.content.dto;

public enum ContentTopicType {
	OUTAGE_MESSAGE("FRC Connect Outage Message");
	
	private String name;
	private ContentTopicType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
