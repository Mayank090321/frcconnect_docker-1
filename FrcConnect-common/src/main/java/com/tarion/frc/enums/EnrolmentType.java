/* 
 * 
 * EnrolmentType.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum EnrolmentType.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum EnrolmentType {
	SINGLE("Single"), MULTIPLE("Multiple");
	
	String desc;
	
	/**
	 * Instantiates a new enrolment type.
	 *
	 * @param desc the desc
	 */
	EnrolmentType(String desc) {
		this.desc = desc;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return desc;
	}
}
