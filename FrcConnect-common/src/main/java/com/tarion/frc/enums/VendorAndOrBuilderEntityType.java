/* 
 * 
 * VendorAndOrBuilderEntityType.java
 *
 * Copyright (c) 2014 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * Used for determining the type of Vendor and/or Builder Entity
 * 
 * Describes the Vendor and/or Builder Entity type - VENDOR_AND_BUILDER, VENDOR, BUILDER
 * 
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 2014-03-20
 * @version 1.0
 *
 */
public enum VendorAndOrBuilderEntityType {
	VENDOR_AND_BUILDER("3"),
	VENDOR("2"),
	BUILDER("1");
	
	private String label;
	
	private VendorAndOrBuilderEntityType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
}