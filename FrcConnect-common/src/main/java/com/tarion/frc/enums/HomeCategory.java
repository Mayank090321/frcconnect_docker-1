/* 
 * 
 * HomeCategory.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum HomeCategory.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum HomeCategory {
	FREEHOLD("Free hold", 1), CONDOMINIUM("Condominium", 2);
	
	String desc;
	Integer code;
	
	/**
	 * Instantiates a new home category.
	 *
	 * @param desc the desc
	 */
	HomeCategory(String desc, Integer code) {
		this.desc = desc;
		this.code = code;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return desc;
	}
	
	public Integer getCode() {
		return code;
	}
}
