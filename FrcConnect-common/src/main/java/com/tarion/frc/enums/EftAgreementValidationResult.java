package com.tarion.frc.enums;

/**
 * The result of EFT Agreement Validation
 *
 * @author <A href="jpaananen@dse-solutions.com">Joni Paananen</A>
 * @since 21-May-2013
 * @version 1.0
 */
public enum EftAgreementValidationResult {
	
	SUCCESS("Success"), 
	TRANSIT_NUMBER("Incorrect format on transit number"),
	ACCOUNT_NUMBER("Incorrect format on bank account number"),
	DEPOSITORY_ID("Incorrect format on depository_id"),
	BANK_NAME("Incorrect format on bank name"),
	BRANCH_NAME("Incorrect format on branch name"),
	DESCRIPTION("Description is longer then 30 characters"),
	FIN("Financial institution number has to be 3-digits number"),
	ACCOUNT_HOLDER_NAME("Account holder name must be 40 characters or less"),
	BANK_ADDRESS("Please enter the bank address"),
	ACCOUNT_HOLDER_EMAIL("Please enter a valid email address"),
	ACCOUNT_HOLDER_TELEPHONE("Invalid telephone number");	

	private String description;
	
	private EftAgreementValidationResult(String desc){
		description = desc;
	}
	
	public String getDescription() {
		return description;
	}
	
}
