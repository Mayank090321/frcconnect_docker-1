/* 
 * 
 * EnrolmentSearchLimitTo.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum EnrolmentSearchLimitTo.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum EnrolmentSearchLimitTo {
	SAVED("Saved but not submitted"), SUBMITTED("Submitted");
	
	String desc;
	
	/**
	 * Instantiates a new enrolment search limit to.
	 *
	 * @param desc the desc
	 */
	EnrolmentSearchLimitTo(String desc) {
		this.desc = desc;
	}

	/**
	 * Gets the desc.
	 *
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Sets the desc.
	 *
	 * @param desc the new desc
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
}
