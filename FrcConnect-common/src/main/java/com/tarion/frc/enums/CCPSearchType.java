package com.tarion.frc.enums;

public enum CCPSearchType {	
	ELIGIBLE_ENROLMENTS("Eligible enrolments"), UPDATED_TODAY_ENROLMENTS("CCPs submitted today"), FUTURE_DATAED_ENROLMENTS("Future-dated CCP submissions");
	
	String desc;
	
	CCPSearchType(String desc) {
		this.desc = desc;
	}
	
	public String toString() {
		return desc;
	}
}
