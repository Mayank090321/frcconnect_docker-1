/**
 * 
 */
package com.tarion.frc.enums;

/**
 * @author <A href="mailto:joni.paananen@tarion.com">Joni Paananen</A>
 *
 * @since Mar 10, 2014
 */
public enum SiteType {
	ALL, FREEHOLD, COMMON_ELEMENT
}
