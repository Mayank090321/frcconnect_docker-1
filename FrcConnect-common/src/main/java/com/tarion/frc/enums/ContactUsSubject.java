/**
 * 
 */
package com.tarion.frc.enums;

/**
 * @author <A href="mailto:joni.paananen@tarion.com">Joni Paananen</A>
 *
 * @since Jul 23, 2014
 */
public enum ContactUsSubject {
	REGISTRATIONS_AND_RENEWALS("registrations"), SECURITY("security"), BUILDERLINK_SUPPORT("support"), ENROLMENTS("enrolments"), CERTIFICATE_OF_EXEMPTION("exemptionCertificate");
	
	private String value;
	
	private ContactUsSubject(String value) {
		this.value = value;
	}
	
	public String toString() {
		return value;
	}
}
