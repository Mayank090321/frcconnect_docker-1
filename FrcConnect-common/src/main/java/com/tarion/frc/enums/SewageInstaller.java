/* 
 * 
 * SewageInstaller.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum SewageInstaller.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum SewageInstaller {
	PURCHASER("2"), VENDOR("1");
	
	String code;
	
	/**
	 * Instantiates a new sewage installer.
	 *
	 * @param code the code
	 */
	SewageInstaller(String code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}
}
