/* 
 * 
 * WarrntyPeriods.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum WarrntyPeriods.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum WarrntyPeriods {
	ALL, INITIAL, REQ, PRE, POST, EXCEP;	
}
