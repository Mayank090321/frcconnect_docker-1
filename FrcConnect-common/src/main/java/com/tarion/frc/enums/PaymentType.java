/**
 * 
 */
package com.tarion.frc.enums;

/**
 * @author <A href="mailto:joni.paananen@tarion.com">Joni Paananen</A>
 *
 * @since Jan 29, 2014
 */
public enum PaymentType {

	ALL, ENROLMENT, REGISTRATION_RENEWAL, OTHER 
}
