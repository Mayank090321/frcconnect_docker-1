/* 
 * 
 * YesNo.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum YesNo.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum YesNo {
	YES("Yes", true), NO("No", false);
	
	String desc;
	Boolean value;
	
	/**
	 * Instantiates a new yes no.
	 *
	 * @param desc the desc
	 * @param value the value
	 */
	YesNo(String desc, Boolean value) {
		this.desc = desc;
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return desc;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public Boolean getValue() {
		return value;
	}
}
