/* 
 * 
 * CondoTypes.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum for Application Status when status change requested
 *
 * @author <A href="bojan.volcansek@tarion.com">Bojan Volcansek</A>
 * @since 13-Feb-2014
 * @version 1.0
 */
public enum ApplicationStatusChange {
	WITHDRAW("WITHDRAW");
	
	String desc;
	
	/**
	 * Instantiates a new enrolment type.
	 *
	 * @param desc the desc
	 */
	ApplicationStatusChange(String desc) {
		this.desc = desc;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return desc;
	}
}
