package com.tarion.frc.enums;

/**
 * User Type enum for use in the user profle.
 * 
 * @author sallen
 *
 */
public enum UserType {
	
	BUILDERLINK_USER("BuilderLink User"),
	GUARANTOR("Guarantor");
	
	String label;
	
	UserType(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
