/* 
 * 
 * CondoTypes.java
 *
 * Copyright (c) 2012 Tarion Warranty Corporation.
 * All rights reserved. 
 * 
 */
package com.tarion.frc.enums;

/**
 * The Enum CondoTypes.
 *
 * @author <A href="mohan.premkumar@tarion.com">Mohan Premkumar</A>
 * @since 13-Sep-2012
 * @version 1.0
 */
public enum CondoTypes {
	TYPEA("Condo-Type A", "7"), TYPEB("Condo-Type B", "8"), TYPEC("Condo-Type C", "9"), TYPED("Condo-Type D", "10");
	
	String desc;
	String code;
	
	/**
	 * Instantiates a new condo types.
	 *
	 * @param desc the desc
	 * @param code the code
	 */
	CondoTypes(String desc, String code) {
		this.desc = desc;
		this.code = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	public String toString() {
		return desc;
	}
}
